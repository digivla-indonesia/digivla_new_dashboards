'use strict';
angular.module('app.filter', [])
angular.module('app.filter').component('filterForms', {
    templateUrl: 'modules/filter/client/views/filter.client.view.html',
    controller: [ 'Authentication', '$http', '$timeout',
        function (Authentication, $http, $timeout) {
            var $ctrl = this;

            $ctrl.hiddenPeriod = true;
            $ctrl.model = {
                gc: '',
                sc: '',
                ms: '',
                mi: '',
                tf: 7,
                df: '',
                dt: '',
                allCategories: [],
                allMedias: []
            };
            $ctrl.dateFrom = '';
            $ctrl.dateTo = '';
            

            function initOptions(){
                // category assets
                $http.get('/api/users/getassets?t=groupcategory').success(function(response) {
                    $ctrl.groupCategories = response.data;
                    $ctrl.selectedGroupCategory = $ctrl.groupCategories[0];
                    getSubCategory($ctrl.groupCategories[0].category_set);
                }).error(function(response) {
                    $ctrl.groupCategories = [{ category_set: '', descriptionz: 'Choose Group Category' }];
                });

                // media assets
                $http.get('/api/users/getassets?t=media').success(function(response) {
                    $ctrl.groupMedias = response.data;
                    $ctrl.selectedGroupMedia = $ctrl.groupMedias[0];
                    getSubMedia($ctrl.selectedGroupMedia.media_set_id);
                }).error(function(response) {
                    $ctrl.groupMedia = [{ media_set_id: '', media_set_name: 'Choose Media Group' }];
                });

                // period assets
                $ctrl.periodOptions = [
                    { name: 'Kemarin', value: 1 },
                    { name: 'Minggu Lalu', value: 7 },
                    { name: 'Bulan Lalu', value: 30 },
                    { name: 'Tahun Lalu', value: 365 },
                    { name: 'Pilih', value: 0 },
                ];
                $ctrl.selectedPeriodOptions = $ctrl.periodOptions[1];


                $timeout(function(){
                    $ctrl.model = {
                        gc: $ctrl.selectedGroupCategory.category_set,
                        sc: $ctrl.selectedSubCategory.category_id,
                        ms: $ctrl.selectedGroupMedia.media_set_id,
                        mi: $ctrl.selectedMediaSelection.user_media_id,
                        tf: 7,
                        df: $ctrl.dateFrom,
                        dt: $ctrl.dateTo,
                        allCategories: $ctrl.allCategories,
                        allMedias: $ctrl.allMedias
                    };
                }, 2500);
            }

            if(Authentication.user !== ""){
                initOptions();
            }



            // category assets
            $ctrl.changeCategorySet = function(){
                $ctrl.model.gc = $ctrl.selectedGroupCategory.category_set;
                getSubCategory($ctrl.selectedGroupCategory.category_set);
            };

            $ctrl.changeSubCategory = function(){
                $ctrl.model.sc = $ctrl.selectedSubCategory.category_id;
            };

            function getSubCategory(cs){
                $http.get('/api/users/getsubclientcategory?cs=' + cs).success(function(response) {
                    var scArr = [{
                      'category_set': '00',
                      'category_id': 'All Category'
                    }];

                    for(var i=0;i<response.length;i++){
                      scArr.push(response[i]);
                    }

                    $ctrl.subCategories = scArr;
                    $ctrl.allCategories = $ctrl.subCategories;
                    $ctrl.model.allCategories = $ctrl.allCategories;
                    $ctrl.selectedSubCategory = $ctrl.subCategories[0];
                }).error(function(response) {
                    $ctrl.subCategories = [{ category_set: '00', category_id: 'Choose Sub' }];
                    $ctrl.selectedSubCategory = $ctrl.subCategories[0];
                });
            };




            // media assets
            $ctrl.changeMediaSet = function(){
                $ctrl.model.ms = $ctrl.selectedGroupMedia.media_set_id;
                getSubMedia($ctrl.selectedGroupMedia.media_set_id);
            };

            $ctrl.changeMediaID = function(){
                $ctrl.model.mi = $ctrl.selectedMediaSelection.user_media_id;
            };

            function getSubMedia(ms){
                $http.get('/api/users/getsubclientmedia?ms=' + ms).success(function (response) {
                    var msArr = [{
                        'user_media_id': '00',
                        'media_name': 'All Media'
                    }];

                    for(var i=0;i<response.length;i++){
                        msArr.push(response[i]);
                    }

                    $ctrl.mediaSelections = msArr;
                    $ctrl.allMedias = $ctrl.mediaSelections;
                    $ctrl.model.allMedias = $ctrl.allMedias;
                    $ctrl.selectedMediaSelection = $ctrl.mediaSelections[0];
                }).error(function (response) {
                    $ctrl.mediaSelection = [{media_set_id: '', media_set_name: 'Choose Media Group'}];
                });
            }




            // period assets
            $ctrl.periodChange = function() {
                $ctrl.model.tf = $ctrl.selectedPeriodOptions.value;
                switch ($ctrl.selectedPeriodOptions.value) {
                    case 0:
                        $ctrl.hiddenPeriod = false;
                        break;
                    default:
                        $ctrl.hiddenPeriod = true;
                        break;
                }
            };


            // datepicker assets
            /* For option 'Pilih' */
            $ctrl.today = function() {
              $ctrl.dt = new Date();
            };
            $ctrl.today();

            $ctrl.clear = function() {
              $ctrl.dt = null;
            };

            $ctrl.inlineOptions = {
              customClass: getDayClass,
              minDate: new Date(),
              showWeeks: true
            };

            $ctrl.dateOptions = {
              formatYear: 'yy',
              maxDate: new Date(),
              minDate: new Date(2010, 1, 1),
              startingDay: 1
            };

            // Disable weekend selection
            // function disabled(data) {
            //   var date = data.date,
            //     mode = data.mode;
            //   return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            // }

            $ctrl.toggleMin = function() {
              $ctrl.inlineOptions.minDate = $ctrl.inlineOptions.minDate ? null : new Date();
              $ctrl.dateOptions.minDate = $ctrl.inlineOptions.minDate;
            };

            $ctrl.toggleMin();

            $ctrl.open1 = function() {
              $ctrl.popup1.opened = true;
            };

            $ctrl.open2 = function() {
              $ctrl.popup2.opened = true;
            };

            $ctrl.setDate = function(year, month, day) {
              $ctrl.dt = new Date(year, month, day);
            };

            $ctrl.formats = ['yyyy-mm-dd', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $ctrl.format = $ctrl.formats[0];
            $ctrl.altInputFormats = ['M!/d!/yyyy'];

            $ctrl.popup1 = {
              opened: false
            };

            $ctrl.popup2 = {
              opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date(tomorrow);
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $ctrl.events = [
              {
                date: tomorrow,
                status: 'full'
              },
              {
                date: afterTomorrow,
                status: 'partially'
              }
            ];

            function getDayClass(data) {
              var date = data.date,
                mode = data.mode;
              if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);

                for (var i = 0; i < $ctrl.events.length; i++) {
                  var currentDay = new Date($ctrl.events[i].date).setHours(0,0,0,0);

                  if (dayToCheck === currentDay) {
                    return $ctrl.events[i].status;
                  }
                }
              }

              return '';
            }

            $ctrl.changeDate = function(datee){
                var formattedDate = datee !== '' ? new Date(datee) : Date.now();

                if (formattedDate) {
                    formattedDate = formattedDate.getFullYear() + '-' + ('0' + (formattedDate.getMonth() + 1)).slice(-2) + '-' + ('0' + formattedDate.getDate()).slice(-2);
                }

                return formattedDate;
            };


        }
    ],
    bindings: {
        model:'=',
        changeFilter: '&',
        parameters: '<'
    }
});


