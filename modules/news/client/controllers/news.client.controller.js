/**=========================================================
 * Module: NGTableCtrl.js
 * Controller for ngTables
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.news')
        .controller('NewsController', NGTableCtrl);
    /*jshint -W055 */
    NGTableCtrl.$inject = ['Authentication', '$filter', 'ngTableParams', '$resource', '$timeout', 'newsDataService', '$rootScope', '$http', '$scope', '$sce', '$q', '$window', '$uibModal', '$log'];
    function NGTableCtrl(Authentication, $filter, ngTableParams, $resource, $timeout, newsDataService, $rootScope, $http, $scope, $sce, $q, $window, $uibModal, $log) {
        var vm = this;
        var deferred = $q.defer();
        var filterBucket = false;
        var td = [];
        vm.selection = [];
        vm.authentication = Authentication;
        vm.title = 'Controller';
        vm.tableParams;
        vm.selectedAll = false;
        vm.totalArticles = 0;

        // Call getTableData()
        vm.initData = function(){
          $timeout(function(){
            vm.tableParams = [];
            vm.loadingData = true;
            getTableData();
          }, 5000);
        };

        vm.reloadTables = function(){
            td = [];
            vm.tableParams = [];
            getTableData();
        };

        vm.trustAsHtml = $sce.trustAsHtml;

        ////////////////

        function activate() {
          var data = [
              {name: 'Moroni',  age: 50, money: -10   },
              {name: 'Tiancum', age: 43, money: 120   },
              {name: 'Jacob',   age: 27, money: 5.5   },
              {name: 'Nephi',   age: 29, money: -54   },
              {name: 'Enos',    age: 34, money: 110   },
              {name: 'Tiancum', age: 43, money: 1000  },
              {name: 'Jacob',   age: 27, money: -201  },
              {name: 'Nephi',   age: 29, money: 100   },
              {name: 'Enos',    age: 34, money: -52.5 },
              {name: 'Tiancum', age: 43, money: 52.1  },
              {name: 'Jacob',   age: 27, money: 110   },
              {name: 'Nephi',   age: 29, money: -55   },
              {name: 'Enos',    age: 34, money: 551   },
              {name: 'Tiancum', age: 43, money: -1410 },
              {name: 'Jacob',   age: 27, money: 410   },
              {name: 'Nephi',   age: 29, money: 100   },
              {name: 'Enos',    age: 34, money: -100  }
          ];

          // SORTING
          // ----------------------------------- 

          vm.data = data;

          vm.tableParams = new ngTableParams({
              page: 1,            // show first page
              count: 10,          // count per page
              sorting: {
                  name: 'asc'     // initial sorting
              }
          }, {
              total: data.length, // length of data
              getData: function($defer, params) {
                  // use build-in angular filter
                  var orderedData = params.sorting() ?
                          $filter('orderBy')(data, params.orderBy()) :
                          data;
          
                  $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
              }
          });

        }


        function getTableData() {
          vm.tableParams = new ngTableParams({
            page: 1,
            count: 25,
            sorting: {
              datee: 'desc'
            }
          }, {
            getData: function($defer, params){

              if(!checkFilterChanging(filterBucket, vm.filterOptions) && td.length > 0){
                  td = params.sorting() ? $filter('orderBy')(td, params.orderBy()) : td;
                  var searchedData = searchData();
                  params.total(searchedData.length);
                  $defer.resolve(searchedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
              } else{
                return newsDataService.postToServer('/api/news/gettablelist', vm.filterOptions, function(response){
                  filterBucket = vm.filterOptions;
                  var results = response.data;
                  td = results.data;
                  td = params.sorting() ? $filter('orderBy')(td, params.orderBy()) : td;
                  var searchedData = searchData();
                  params.total(searchedData.length); 

                  $defer.resolve(searchedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                });
              }

            }
          });

          // return;
        }


        vm.checkSelectionExists = function(article){
          var found = false;
          for(var i=0;i<vm.selection.length;i++) {
            if(vm.selection[i].article_id.toString() === article.article_id.toString() && (vm.selection[i].category_id.length == article.categories.length && vm.selection[i].category_id.every(function(u, i) {return u === article.categories[i]})) && (vm.selection[i]._id.length == article._id.length && vm.selection[i]._id.every(function(u, i) {return u === article._id[i]})) ){
              found = true;
              break;
            }
          }

          return found;
        };

        vm.toggleSelection = function toggleSelection(article) {
          var found = false;
          var index = null;
          
          if(vm.selection.length > 0){
            for(var i=0;i<vm.selection.length;i++) {
              if(vm.selection[i].article_id.toString() === article.article_id.toString() && (vm.selection[i].category_id.length == article.categories.length && vm.selection[i].category_id.every(function(u, i) {return u === article.categories[i]})) && (vm.selection[i]._id.length == article._id.length && vm.selection[i]._id.every(function(u, i) {return u === article._id[i]})) ){
                  index = i;
                  break;
              }
            }
          }

          // is currently selected
          if(index !== null){
            vm.selection.splice(index, 1);
          // is newly selected
          } else{
            vm.selection.push({
              _id: article._id,
              article_id: article.article_id,
              category_id: article.categories,
              tone: article.tone
            });
          }

          if(vm.selection.length > 0){
            vm.showButtonEmail = true;
          } else{
            vm.showButtonEmail = false;
          }
        };


        $scope.$watch(function(){
          return vm.searchTable;
        }, function () {
          if(vm.tableParams)
            vm.tableParams.reload();
        });

        function searchData(){
          if(vm.searchTable)
             return $filter('filter')(td,vm.searchTable);
          return td;
        }

        function checkFilterChanging(ofo, nfo){
          if( ofo.gc === nfo.gc && ofo.sc === nfo.sc && ofo.ms === nfo.ms && ofo.mi === nfo.mi && ofo.tf === nfo.tf && ofo.df === nfo.df && ofo.dt === nfo.dt ){
            return false;
          } else{
            return true;
          }
        }

        vm.highlightKeyword = function(article){
          var temp = article.detail.content;
          var keywords = article.keywords;
          var selectedKeyword;
          var x, y;
          for(var j=0;j<keywords.length;j++){
            selectedKeyword = keywords[j].match(/\w+|"[^"]+"/g);
            for(var k=0;k<selectedKeyword.length;k++){
              x = selectedKeyword[k];
              x = x.replace(/\"/g, "");
              x = x.replace(/\'/g, "");

              temp = temp.replace(new RegExp('('+x+')', "ig"), "<span style='color:#f89406; font-weight:bolder;'>$1</span>");
            }
          }

          return temp.replace(/(?:\r\n|\r|\n)/g, '<br />');
        }

        vm.checkExpanded = function(article){

          if(article.expanded) article.expanded = false;
          else article.expanded = true;
        }

        vm.checkAll = function(){ alert('checkAll');
          if(!vm.selectedAll) {
            vm.selectedAll = true;
            angular.forEach(td, function (value, key) {
              if(!vm.checkSelectionExists(value)){
                vm.selection.push({
                  _id: value._id,
                  article_id: value.article_id,
                  category_id: value.categories
                });
              }
            });
          } else {
            vm.selectedAll = false;
            // for(var j=0;j<$scope.tableData.length;j++){
            //   for(var i=0;i<$scope.selection.length;i++){
            //     if($scope.tableData[j].article_id.toString() === $scope.selection[i].article_id.toString()){
            //       $scope.selection.splice(i, 1);
            //       break;
            //     }
            //   }
            // }
            vm.selection = [];
          }
        };

        vm.saveActivity = function(article, type){
          var items = [{
            article_id: article.article_id,
            category_id: article.category_id,
            activity: type
          }];

          newsDataService.postToServer('/api/news/saveactivity', items, function(response){
            return;
          });
        };

        vm.getPDF = function(type, article){
          var items = {
            type: type,
            article_id: article.detail.article_id,
            article_detail: article.detail
          };
          // return console.log(JSON.stringify(items));
          // $http.post('/api/news/generatepdf').success(function(data, status, headers, config){
          newsDataService.postToServer('/api/news/generatepdf', items, function(response){
            // return;
            // console.log(JSON.stringify(response));
            var fn = 'pdf_' + Date.now().toString() + '.pdf';
            pdfMake.createPdf(response.data).download(fn);
          });
          
        };

        vm.originaleSingle = function(stat, formate, article, type){
          var items = [{
            article_id: article.article_id,
            category_id: article.category_id,
            activity: type
          }];
          

          newsDataService.postToServer('/api/news/saveactivity', items, function(response){
            var url = 'http://pdf.antara-insight.id?stat=' + stat;
            url += '&article=' + article.article_id;
            url += '&formate=' + formate;
            url += '&logo_head=' + Authentication.user.user_detail.logo_head;
            url += '&client_id=' + Authentication.user.user_detail.client_id;

            $window.open(url, '_blank');
          });

        };

        vm.openModal = function(dest) {
          $.fancybox.open({href: '#' + dest});
        };

        vm.emailAll = function(){
          var items = {
            article_id: vm.selection,
            email: vm.destinationEmail,
            media_set: vm.filterOptions.ms
          };

          newsDataService.postToServer('/api/news/sendemail', items, function(response){
            alert('Email Sent!');
            $.fancybox.close();
          });

        };

        vm.originale = function(stat, formate, type){
          var articles = [];
          for(var i=0;i<vm.selection.length;i++){
            articles.push(vm.selection[i].article_id);
            vm.selection[i].activity = type;
          }

          newsDataService.postToServer('/api/news/saveactivity', vm.selection, function(response){
            var url = 'http://pdf.antara-insight.id?stat=' + stat;
            url += '&article=' + articles.join();
            url += '&formate=' + formate;
            url += '&logo_head=' + Authentication.user.user_detail.logo_head;
            url += '&client_id=' + Authentication.user.user_detail.client_id;

            $window.open(url, '_blank');
          });

        };

        vm.open = function (items) {

          var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'newsModal.html',
            controller: 'NewsModalController',
            // size: size,
            resolve: {
              items: function () {
                // return vm.items;
                // return items;
                return {
                  type: 'email',
                  article_id: vm.selection,
                  // email: vm.destinationEmail,
                  email: '',
                  media_set: vm.filterOptions.ms
                };

              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            vm.selected = selectedItem;
          }, function () {
            $log.info('Modal dismissed at: ' + new Date());
          });
        };

    }
})();


