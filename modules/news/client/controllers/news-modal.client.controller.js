/**=========================================================
 * Module: NGTableCtrl.js
 * Controller for ngTables
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.news')
        .controller('NewsModalController', ModalCtrl);
    /*jshint -W055 */
    ModalCtrl.$inject = ['Authentication', '$scope', '$uibModalInstance', 'newsDataService', 'items', '$log'];
    function ModalCtrl(Authentication, $scope, $uibModalInstance, newsDataService, items, $log) {

        $scope.items = items;
        // $scope.selected = {
        //   item: $scope.items[0]
        // };

        $scope.ok = function () {
          $uibModalInstance.close($scope.selected.item);
        };

        $scope.cancel = function () {
          $uibModalInstance.dismiss('cancel');
        };


        $scope.emailAll = function(){
          newsDataService.postToServer('/api/news/sendemail', $scope.items, function(response){
            alert('Email Sent!');
            $uibModalInstance.close('Email Sent!');
          });
        }
    }
})();


