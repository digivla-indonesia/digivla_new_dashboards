(function() {
    'use strict';

    angular
        .module('app.news')
        .config(appRoutes);
    appRoutes.$inject = ['$stateProvider', 'RouteHelpersProvider'];

    function appRoutes($stateProvider, helper) {

        $stateProvider
            .state('app.news', {
                url: '/news',
                templateUrl: 'modules/news/client/views/news.client.view.html',
                resolve: helper.resolveFor('ngTable', 'ngTableExport')
            });
            // .state('app.editing-standard', {
            //     url: '/editing/standard',
            //     title: 'Table Standard',
            //     templateUrl: 'modules/editings/client/views/table-standard.client.view.html'
            // })
            // .state('app.editing-extended', {
            //     url: '/editing/extended',
            //     title: 'Table Extended',
            //     templateUrl: 'modules/editings/client/views/table-extended.client.view.html'
            // })
            // .state('app.editing-datatable', {
            //     url: '/editing/datatable',
            //     title: 'Table Datatable',
            //     templateUrl: 'modules/editings/client/views/table-datatable.client.view.html',
            //     resolve: helper.resolveFor('datatables')
            // })
            // .state('app.editing-xeditable', {
            //     url: '/editing/xeditable',
            //     templateUrl: 'modules/editings/client/views/table-xeditable.client.view.html',
            //     resolve: helper.resolveFor('xeditable')
            // })
            // .state('app.editing-ngtable', {
            //     url: '/editing/ngtable',
            //     templateUrl: 'modules/editings/client/views/table-ngtable.client.view.html',
            //     resolve: helper.resolveFor('ngTable', 'ngTableExport')
            // })
            // .state('app.editing-uigrid', {
            //     url: '/editing/uigrid',
            //     templateUrl: 'modules/editings/client/views/table-uigrid.client.view.html',
            //     resolve: helper.resolveFor('ui.grid')
            // })
            // .state('app.editing-angulargrid', {
            //     url: '/editing/angulargrid',
            //     templateUrl: 'modules/editings/client/views/table-angulargrid.client.view.html',
            //     resolve: helper.resolveFor('angularGrid')
            // });

    }
})();