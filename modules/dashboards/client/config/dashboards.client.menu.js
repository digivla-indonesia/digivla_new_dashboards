(function() {
    'use strict';

    angular
        .module('app.dashboards', ['highcharts-ng', 'angular-loading-bar'])
        .run(coreMenu);

    coreMenu.$inject = ['Menus'];
    function coreMenu(Menus){

        Menus.addMenuItem('sidebar', {
            title: 'Dashboards',
            state: 'app.dashboards',
            iconClass: 'icon-graph',
            position: 6,
            roles: ['*']
        });
        // Menus.addMenuItem('sidebar', {
        //     title: 'Dashboards',
        //     state: 'app.dashboards',
        //     type: 'dropdown',
        //     iconClass: 'icon-graph',
        //     position: 6,
        //     roles: ['*']
        // });
        // Menus.addSubMenuItem('sidebar', 'app.dashboards', {
        //     title: 'Flot',
        //     state: 'app.dashboards-flot',
        // });
        // Menus.addSubMenuItem('sidebar', 'app.dashboards', {
        //     title: 'Radial',
        //     state: 'app.dashboards-radial',
        // });
        // Menus.addSubMenuItem('sidebar', 'app.dashboards', {
        //     title: 'Chartjs',
        //     state: 'app.dashboards-js',
        // });
        // Menus.addSubMenuItem('sidebar', 'app.dashboards', {
        //     title: 'Rickshaw',
        //     state: 'app.dashboards-rickshaw',
        // });
        // Menus.addSubMenuItem('sidebar', 'app.dashboards', {
        //     title: 'Morris',
        //     state: 'app.dashboards-morris',
        // });
        // Menus.addSubMenuItem('sidebar', 'app.dashboards', {
        //     title: 'Chartist',
        //     state: 'app.dashboards-chartist',
        // });

    }

})();
