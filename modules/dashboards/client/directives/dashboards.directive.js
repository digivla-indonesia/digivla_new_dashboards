/**=========================================================
 * Module: dashboard.js
 * Initializes the dashboard chart plugin and handles data refresh
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.dashboards')
        .directive('chart', dashboard);

    dashboard.$inject = ['$http', '$timeout'];

    function dashboard($http, $timeout) {

        var directive = {
            restrict: 'EA',
            template: '<div></div>',
            scope: {
                dataset: '=?',
                options: '=',
                series: '=',
                callback: '=',
                src: '='
            },
            link: link
        };
        return directive;

        function link(scope, element, attrs) {
            var height, plot, plotArea, width;
            var heightDefault = 220;

            plot = null;

            width = attrs.width || '100%';
            height = attrs.height || heightDefault;

            plotArea = $(element.children()[0]);
            plotArea.css({
                width: width,
                height: height
            });

            function init() {
                var plotObj;
                if (!scope.dataset || !scope.options) return;
                plotObj = $.plot(plotArea, scope.dataset, scope.options);
                scope.$emit('plotReady', plotObj);
                if (scope.callback) {
                    scope.callback(plotObj, scope);
                }

                return plotObj;
            }

            function onDatasetChanged(dataset) {
                if (plot) {
                    plot.setData(dataset);
                    plot.setupGrid();
                    return plot.draw();
                } else {
                    plot = init();
                    onSerieToggled(scope.series);
                    return plot;
                }
            }
            var $watchOff1 = scope.$watchCollection('dataset', onDatasetChanged, true);

            function onSerieToggled(series) {
                if (!plot || !series) return;
                var someData = plot.getData();
                for (var sName in series) {
                    angular.forEach(series[sName], toggleFor(sName));
                }

                plot.setData(someData);
                plot.draw();

                function toggleFor(sName) {
                    return function(s, i) {
                        if (someData[i] && someData[i][sName])
                            someData[i][sName].show = s;
                    };
                }
            }
            var $watchOff2 = scope.$watch('series', onSerieToggled, true);

            function onSrcChanged(src) {

                if (src) {

                    $http.get(src)
                        .then(function(data) {

                            $timeout(function() {
                                scope.dataset = data.data;
                            });

                        }, function() {
                            $.error('dashboard chart: Bad request.');
                        });

                }
            }
            var $watchOff3 = scope.$watch('src', onSrcChanged);

            scope.$on('$destroy', function(){
                // detach watches and scope events
                $watchOff1();
                $watchOff2();
                $watchOff3();
                // destroy chart
                plot.destroy();
            });


            // click event
            var getKeyByValue = function(obj, value ) {
                for( var prop in obj ) {
                    if( obj.hasOwnProperty( prop ) ) {
                         if( obj[ prop ] === value )
                             return prop;
                    }
                }
            };

            element.bind("plotclick", function (event, pos, item) {
                if(item){
                    // scope.dis= getKeyByValue(item.series.xaxis.categories, item.dataIndex);
                    // scope.$apply();
                    alert(JSON.stringify(item));
               }
            });

        }
    }


})();
