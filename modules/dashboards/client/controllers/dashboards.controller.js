/**=========================================================
 * Module: flot-chart.js
 * Setup options and data for flot chart directive
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.dashboards')
        .controller('DashboardsController', DashboardsController);

    // DashboardsController.$inject = ['$scope', '$http', '$state', 'Authentication', 'ChartData', '$timeout', 'Colors', '$rootScope', 'ArticleListData', '$modal', '$log', 'FileSaver', 'Blob'];
    // function DashboardsController($scope, $http, $state, Authentication, ChartData, $timeout, Colors, $rootScope, ArticleListData, $modal, $log, FileSaver, Blob) {
    DashboardsController.$inject = ['$scope', '$http', '$state', 'Authentication', 'ChartData', '$timeout', 'Colors', '$rootScope', 'ArticleListData', '$uibModal', '$log', 'FileSaver', 'Blob'];
    function DashboardsController($scope, $http, $state, Authentication, ChartData, $timeout, Colors, $rootScope, ArticleListData, $uibModal, $log, FileSaver, Blob) {
        var vm = this;
        var mediaVisibilityChartData, summaryChartData, toneByCategoryChartData, coverageTonalityChartData, mediaSelectionChartData, EWSChartData;

        if(Authentication.user === ""){
          return $state.go("page.authentication.signin");
        }

        vm.initData = function(){
          $timeout(function(){
            activate();
          }, 5000);
        };


        vm.reloadCharts = function(){
          $timeout(function(){
            getChartsData();
          }, 1000)
        };


        ////////////////

        function activate() {
          getChartsData();

          // PANEL REFRESH EVENTS
          // -----------------------------------

          $scope.$on('panel-refresh', function(event, id) {

            console.log('Simulating chart refresh during 3s on #'+id);

            // Instead of timeout you can request a chart data
            $timeout(function(){

              // directive listen for to remove the spinner
              // after we end up to perform own operations
              $scope.$broadcast('removeSpinner', id);

              console.log('Refreshed #' + id);

            }, 3000);

          });


          // PANEL DISMISS EVENTS
          // -----------------------------------

          // Before remove panel
          $scope.$on('panel-remove', function(event, id, deferred){

            console.log('Panel #' + id + ' removing');

            // Here is obligatory to call the resolve() if we pretend to remove the panel finally
            // Not calling resolve() will NOT remove the panel
            // It's up to your app to decide if panel should be removed or not
            deferred.resolve();

          });

          // Panel removed ( only if above was resolved() )
          $scope.$on('panel-removed', function(event, id){

            console.log('Panel #' + id + ' removed');

          });

        }


        function getChartsData(){
          var toneParams = vm.filterOptions;
          toneParams.code = '';
          var toneChart = ChartData.getPanelData('/api/dashboards/getcountbytone?', toneParams, processToneChart);

          var summaryParams = vm.filterOptions;
          summaryParams.code = '';
          var summaryChart = ChartData.getPanelData('/api/dashboards/getprocess?', summaryParams, processSummaryChart);

          var mediaVisibilityParams = vm.filterOptions;
          mediaVisibilityParams.code = '';
          var mediaVisibilityChart = ChartData.getPanelData('/api/dashboards/getprocess4?', mediaVisibilityParams, processMediaVisibilityChart);

          var toneByCategoryParams = vm.filterOptions;
          toneByCategoryParams.code = '019';
          var toneByCategoryChart = ChartData.getPanelData('/api/dashboards/getprocess3?', toneByCategoryParams, processToneByCategory);

          var coverageTonalityParams = vm.filterOptions;
          coverageTonalityParams.code = '018';
          var toneByCategoryChart = ChartData.getPanelData('/api/dashboards/getprocess3?', coverageTonalityParams, processCoverageTonality);

          var mediaSelectionParams = vm.filterOptions;
          mediaSelectionParams.code = '050';
          var mediaSelectionChart = ChartData.getPanelData('/api/dashboards/getprocess3?', mediaSelectionParams, processMediaSelection);

          var EWSChart = ChartData.EWSChartData(vm.filterOptions, processEWS);
        }


        function processToneChart(response){
          var tones = ['-1', '0', '1'];

          for(var i=0;i<tones.length;i++){
            if(tones[i] === '1') vm.positiveChartOptions = gaugeMaker({tone: tones[i], percentage: response[tones[i]].percentage});
            else if(tones[i] === '0') vm.neutralChartOptions = gaugeMaker({tone: tones[i], percentage: response[tones[i]].percentage});
            if(tones[i] === '-1') vm.negativeChartOptions = gaugeMaker({tone: tones[i], percentage: response[tones[i]].percentage});
          }
        }

        function gaugeMaker(items){
          var colors, titles = '';

          if(items.tone === '1'){ 
            colors = ['#37b550', '#ffffff'];
            titles = 'Positive';
          } else if(items.tone === '0'){
            colors = ['#00a0da', '#ffffff'];
            titles = 'Neutral';
          } else if(items.tone === '-1'){
            colors = ['#ee3048', '#ffffff'];
            titles = 'Negative';
          }

          var params = {
            options: {
              plotOptions: {
                pie: {
                    borderColor: '#000000',
                    size: 115,
                    dataLabels: {
                        enabled: false,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white',
                            textShadow: '0px 1px 2px black',
                            
                        }
                        
                    },
                    startAngle: -90,
                    endAngle: 90
                }
            },
                colors: colors,
                chart: {
                    type: 'pie',
                    height: 125
                }
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: titles,
                style: {
                    color: colors[0],
                    fontWEight: 'bold',
                    fontSize: '15px'
                },
                verticvalAlign: 'middle',
                y: 5,
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'  
            },
            
            series: [{
                type: 'pie',
                name: 'Loan',
                innerSize: '50%',
                data: [
                    [items.percentage + '% ' + titles, items.percentage],
                    [(100 - items.percentage).toString() + '% Lainnya', 100 - items.percentage]
                ]
            }],
            loading: false
          };

          return params;
        }


        function processMediaVisibilityChart(response){
          mediaVisibilityChartData = response;
          vm.mediaVisibilityConfig = {
            options: {
              chart: {
                type: 'area'
              },
              plotOptions: {
                series: {
                  cursor: 'pointer',
                  point: {
                    events: {
                      click: function() {
                        vm.open({
                          type: '4',
                          date_to: this.category,
                          date_from: this.category,
                          media_set: vm.filterOptions.ms,
                          media_id: vm.filterOptions.mi,
                          group_category: vm.filterOptions.gc,
                          sub_category: this.series.name,
                          tone: '',
                          time_frame: vm.filterOptions.tf,
                          original_date_from: vm.filterOptions.df,
                          original_date_to: vm.filterOptions.dt,
                          old_url: ''
                        });
                      }
                    }
                  }
                }
              }
            },
            xAxis: {
              title: {
                text: 'Date'
              },
              categories: response.categories
            },
            yAxis: {
              title: {
                text: 'Number of Article'
              },
              min: 0
            },
            series: response.series,
            title: {
              text: ''
            },
            size: {
              height: 300
            },
            loading: false
          };
        }


        function processSummaryChart(response){
          summaryChartData = response;
          vm.summaryConfig = {
            options: {
              chart: {
                type: 'pie',
                options3d: {
                  enabled: true,
                  alpha: 45,
                  beta: 0
                }
              },
              plotOptions: {
                pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  depth: 35,
                  dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                  }
                }
              }
            },
            title: {
              text: ''
            },
            tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            size: {
              height: 300,
              width: 543
            },
            series: [{
              type: 'pie',
              name: 'Mentions',
              data: response,
              point: {
                events: {
                  click: function(event) {
                    vm.open({
                      type: '0',
                      date_to: vm.filterOptions.dt,
                      date_from: vm.filterOptions.df,
                      media_set: vm.filterOptions.ms,
                      media_id: vm.filterOptions.mi,
                      group_category: vm.filterOptions.gc,
                      sub_category: this.name,
                      tone: '',
                      time_frame: vm.filterOptions.tf,
                      original_date_from: vm.filterOptions.df,
                      original_date_to: vm.filterOptions.dt,
                      old_url: ''
                    });
                  }
                }
              }
            }]
          };
        }


        function processToneByCategory(response){
          toneByCategoryChartData = response;
          vm.toneByCategoryConfig = {
            title: {
              text: ''
            },
            xAxis: {
              categories: response.category
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Number of Article'
              },
              stackLabels: {
                enabled: true,
                style: {
                  fontWeight: 'bold',
                  color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
              }
            },
            options: {
              chart: {
                type: 'column'
              },
              colors: ['#37b550', '#00a0da', '#ee3048'],
              legend: {
                align: 'right',
                x: 0,
                verticalAlign: 'top',
                y: -10,
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
              },
              tooltip: {
                formatter: function() {
                  return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
                }
              },
              plotOptions: {
                column: {
                  stacking: 'normal',
                  dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                      textShadow: '0 0 3px black, 0 0 3px black'
                    }
                  }
                },
                series: {
                  cursor: 'pointer',
                  point: {
                    events: {
                      click: function() {
                        vm.open({
                          type: '019',
                          date_to: '',
                          date_from: '',
                          media_set: vm.filterOptions.ms,
                          media_id: vm.filterOptions.mi,
                          group_category: vm.filterOptions.gc,
                          sub_category: this.category,
                          tone: this.series.name,
                          original_date_from: vm.filterOptions.df,
                          original_date_to: vm.filterOptions.dt,
                          old_url: ''
                        });
                      }
                    }
                  }
                }
              }
            },
            series: response.series,
            size: {
              height: 300
            }
          };
        }


        function processCoverageTonality(response){
          coverageTonalityChartData = response;
          vm.coverageTonalityConfig = {
            title: {
              text: ''
            },
            xAxis: {
              categories: response.category
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Number of Article'
              },
              stackLabels: {
                enabled: true,
                style: {
                  fontWeight: 'bold',
                  color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
              }
            },
            options: {
              chart: {
                type: 'column'
              },
              colors: ['#37b550', '#00a0da', '#ee3048'],
              legend: {
                align: 'right',
                x: 0,
                verticalAlign: 'top',
                y: -10,
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
              },
              tooltip: {
                formatter: function() {
                  return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
                }
              },
              plotOptions: {
                column: {
                  stacking: 'normal',
                  dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                      textShadow: '0 0 3px black, 0 0 3px black'
                    }
                  }
                },
                series: {
                  cursor: 'pointer',
                  point: {
                    events: {
                      click: function() {
                        vm.open({
                          type: '018',
                          date_to: this.category,
                          date_from: this.category,
                          media_set: vm.filterOptions.ms,
                          media_id: vm.filterOptions.mi,
                          group_category: vm.filterOptions.gc,
                          sub_category: vm.filterOptions.sc,
                          tone: this.series.name,
                          original_date_from: vm.filterOptions.df,
                          original_date_to: vm.filterOptions.dt,
                          old_url: ''
                        });
                      }
                    }
                  }
                }
              }
            },
            series: response.series,
            size: {
              height: 300
            }
          };
        }


        function processMediaSelection(response){
          mediaSelectionChartData = response;
          vm.mediaList = response.category_detail;
          vm.mediaSelectionConfig = {
            title: {
              text: ''
            },
            xAxis: {
              categories: response.category
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Number of Article'
              },
              stackLabels: {
                enabled: true,
                style: {
                  fontWeight: 'bold',
                  color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
              }
            },
            options: {
              chart: {
                type: 'column'
              },
              colors: ['#37b550', '#00a0da', '#ee3048'],
              legend: {
                align: 'right',
                x: 0,
                verticalAlign: 'top',
                y: -10,
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
              },
              tooltip: {
                formatter: function() {
                  return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
                }
              },
              plotOptions: {
                column: {
                  stacking: 'normal',
                  dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                      textShadow: '0 0 3px black, 0 0 3px black'
                    }
                  }
                },
                series: {
                  cursor: 'pointer',
                  point: {
                    events: {
                      click: function() {
                        vm.open({
                          type: '050',
                          date_to: '',
                          date_from: '',
                          media_set: vm.filterOptions.ms,
                          media_id: {
                            name: this.category,
                            id: vm.mediaList[this.category]
                          },
                          group_category: vm.filterOptions.gc,
                          sub_category: vm.filterOptions.sc,
                          tone: this.series.name,
                          original_date_from: vm.filterOptions.df,
                          original_date_to: vm.filterOptions.dt,
                          old_url: ''
                        });
                      }
                    }
                  }
                }
              }
            },
            series: response.series,
            size: {
              height: 300
            }
          };
        }


        function processEWS(response){
          var EWSData = response.data.data;
          var EWSResult = [];
          var arrDate, JSONResult = [], temporaryJSON;
          var sentimentLabels = ["0", "Potential", "Emerging", "Current", "Crisis"];

          var from = (vm.filterOptions.df !== '') ? new Date(vm.filterOptions.df) : '',
              to = (vm.filterOptions.dt !== '') ? new Date(vm.filterOptions.dt) : '';

          if (from !== '' && to !== '') {
            from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
            to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
          }

          var dateFormatting = function(timeFrame, df, dt){
            var currentDate = new Date(),
              dateFlag = new Date(),
              interval = 'day',
              date_from = df,
              date_to = dt,
              tmpArray = [];

            var from = (date_from !== '') ? new Date(date_from) : '',
                to = (date_to !== '') ? new Date(date_to) : '';

            if (from !== '' && to !== '') {
                from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
                to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
            }

            var dateFormatter = function(tgl){
              return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
            }

              if(timeFrame !== 0){
                switch(timeFrame){
                    case 1:
                      tmpArray.push(dateFormatter(dateFlag));
                      for(var i=0;i<1;i++){
                        dateFlag.setDate(dateFlag.getDate() - 1);
                        tmpArray.push(dateFormatter(dateFlag));
                      }
                      break;
                    case 7:
                      tmpArray.push(dateFormatter(dateFlag));
                      for(var i=0;i<6;i++){
                        dateFlag.setDate(dateFlag.getDate() - 1);
                        tmpArray.push(dateFormatter(dateFlag));
                      }
                      break;
                    case 30:
                      tmpArray.push(dateFormatter(dateFlag));
                      for(var i=0;i<29;i++){
                        dateFlag.setDate(dateFlag.getDate() - 1);
                        tmpArray.push(dateFormatter(dateFlag));
                      }
                      break;
                    case 365:
                      tmpArray.push(dateFormatter(dateFlag));
                      for(var i=0;i<364;i++){
                        dateFlag.setDate(dateFlag.getDate() - 1);
                        tmpArray.push(dateFormatter(dateFlag));
                      }
                      break;
                }
            } else{
              var oneDay = 24*60*60*1000;
              var firstDate = new Date(date_from);
              var secondDate = new Date(date_to);
              var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

              for(var i=0;i<diffDays;i++){
                dateFlag.setDate(dateFlag.getDate() - 1);
                tmpArray.push(dateFormatter(dateFlag));
              }
            }

            return tmpArray;
          };

          arrDate = dateFormatting(vm.filterOptions.tf, from, to).sort();

          for(var i=0;i<EWSData.length;i++){
            var dateDataContainer = [];
            temporaryJSON = {};
            temporaryJSON.name = EWSData[i].category_id;

            for(var j=0;j<arrDate.length;j++){
              dateDataContainer.push(parseFloat(EWSData[i][arrDate[j]]));
            }

            temporaryJSON.data = dateDataContainer;

            JSONResult.push(temporaryJSON);
          }
          
          EWSChartData = {
            categories: arrDate,
            series: JSONResult
          };

          vm.EWSChartConfig = {
            title: {
              text: ''
            },
            tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            size: {
              height: 300
            },
            xAxis: {
              title: {
                text: 'Date'
              },
              categories: arrDate
            },
            yAxis: {
              title: {
                text: 'Stage'
              },
              labels: {
                formatter: function(){
                  return sentimentLabels[this.value];
                }
              },
              tickInterval: 1,
              min: 0,
              max: 4
            },
            series: JSONResult,
            options: {
              chart: {
                type: 'line'
              },
              plotOptions: {
                series: {
                  cursor: 'pointer',
                  point: {
                    events: {
                      click: function() {
                        vm.open({
                          type: 'ews',
                          date_to: this.category,
                          date_from: this.category,
                          media_set: vm.filterOptions.ms,
                          media_id: vm.filterOptions.mi,
                          group_category: vm.filterOptions.gc,
                          sub_category: vm.filterOptions.sc,
                          tone: this.series.name,
                          original_date_from: vm.filterOptions.df,
                          original_date_to: vm.filterOptions.dt,
                          old_url: ''
                        });
                      }
                    }
                  }
                }
              }
            },
          };
        }

        vm.open = function (items) {

          var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'dashboardList.html',
            controller: 'DashboardsListArticleController',
            size: 'lg',
            resolve: {
              items: function () {
                return items;
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            vm.selected = selectedItem;
          }, function () {
            $log.info('Modal dismissed at: ' + new Date());
          });
        };

        vm.downloadExcel = function(){
          var url = '/api/dashboards/downloadexcel?' +
            'gc=' + vm.filterOptions.gc +
            '&ms=' + vm.filterOptions.ms +
            '&sc=' + vm.filterOptions.sc +
            '&mi=' + vm.filterOptions.mi +
            '&tf=' + vm.filterOptions.tf +
            '&df=' + vm.filterOptions.df +
            '&dt=' + vm.filterOptions.dt;

          var mediaVisibilityChart = ChartData.getPanelData2('/api/dashboards/downloadexcel?', vm.filterOptions, function(response){
            // var results = response.data;
            // var ws_name = "Digivla Indonesia";
            // var wb = new Workbook(), ws = sheet_from_json_array(results);
            // wb.SheetNames.push(ws_name);
            // wb.Sheets[ws_name] = ws;
            // console.log(JSON.stringify(wb));
            // var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
            // var filename = "summary - " + new Date() + ".xlsx";
            // FileSaver.saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), filename);

            var filename = "summary - " + new Date() + ".xlsx";
            var dataLength = response.data.length;
            var ws = XLSX.utils.json_to_sheet(response.data);

            // create hyperlink
            for(var i=2;i<dataLength+2;i++){
              // ws['H' + i.toString()].l = {Target: response.data[i-2].file_pdf, Tooltip: "Link"};
              delete ws['H' + i.toString()].t;
              ws['H' + i.toString()].v = response.data[i-2].file_pdf;
              ws['H' + i.toString()].f = '=HYPERLINK("'+ response.data[i-2].file_pdf +'","Link")';
            }

            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, "Digivla Indonesia");
            var wbout = XLSX.write(wb, {bookType:'xlsx', type:'binary'});
            FileSaver.saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), filename);
          });
          /*******************/
          /* original data */
        };

        function Workbook() {
          if(!(this instanceof Workbook)) return new Workbook();
          this.SheetNames = [];
          this.Sheets = {};
        }

        function sheet_from_json_array(data, opts){
          var ws = {};
          // s = start of range, e = end of range
          // c = column, r = row
          var range = {s: {c:0, r:10000000}, e: {c:0, r:0}};
          var cell_ref = XLSX.utils.encode_cell({c: 0, r: 0});
          ws[cell_ref] = {
            v: 'No',
            t: 's',
            s: {
              patternType: 'solid',
              fgColor: {
                theme: 8,
                tint: 0.3999755851924192,
                rgb: '9ED2E0'
              },
              bgColor: {
                indexed: 64
              }
            }
          };

          cell_ref = XLSX.utils.encode_cell({c: 1, r: 0});
          ws[cell_ref] = {v: 'Date', t: 's'};

          cell_ref = XLSX.utils.encode_cell({c: 2, r: 0});
          ws[cell_ref] = {v: 'Client Set', t: 's'};

          cell_ref = XLSX.utils.encode_cell({c: 3, r: 0});
          ws[cell_ref] = {v: 'Sub Client Set', t: 's'};

          cell_ref = XLSX.utils.encode_cell({c: 4, r: 0});
          ws[cell_ref] = {v: 'Title', t: 's'};

          cell_ref = XLSX.utils.encode_cell({c: 5, r: 0});
          ws[cell_ref] = {v: 'Media Type', t: 's'};

          cell_ref = XLSX.utils.encode_cell({c: 6, r: 0});
          ws[cell_ref] = {v: 'Media Name', t: 's'};

          cell_ref = XLSX.utils.encode_cell({c: 7, r: 0});
          ws[cell_ref] = {v: 'Page', t: 's'};

          cell_ref = XLSX.utils.encode_cell({c: 8, r: 0});
          ws[cell_ref] = {v: 'Source', t: 's'};

          cell_ref = XLSX.utils.encode_cell({c: 9, r: 0});
          ws[cell_ref] = {v: 'Tone', t: 's'};

          cell_ref = XLSX.utils.encode_cell({c: 10, r: 0});
          ws[cell_ref] = {v: 'Journalist', t: 's'};

          cell_ref = XLSX.utils.encode_cell({c: 11, r: 0});
          ws[cell_ref] = {v: 'PR Value', t: 's'};

          cell_ref = XLSX.utils.encode_cell({c: 12, r: 0});
          ws[cell_ref] = {v: 'Ad Value', t: 's'};

          range.s.r = 0;
          range.e.c = 14;

          for(var R=0;R<data.length;R++){
            if(range.s.r > R) range.s.r = R+1;
            if(range.e.r < R) range.e.r = R+1;

            var cell = {v: R+1, t: 'n'};
            var cell_ref = XLSX.utils.encode_cell({c: 0, r: R+1});
            ws[cell_ref] = cell;

            cell = {v: data[R].datee, t: 's'};
            cell_ref = XLSX.utils.encode_cell({c: 1, r: R+1});
            ws[cell_ref] = cell;

            cell = {v: vm.filterOptions.gc, t: 's'};
            cell_ref = XLSX.utils.encode_cell({c: 2, r: R+1});
            ws[cell_ref] = cell;

            cell = {v: data[R].category_id, t: 's'};
            cell_ref = XLSX.utils.encode_cell({c: 3, r: R+1});
            ws[cell_ref] = cell;

            cell = {v: data[R].title, t: 's'};
            cell_ref = XLSX.utils.encode_cell({c: 4, r: R+1});
            ws[cell_ref] = cell;

            cell = {v: data[R].media_type, t: 's'};
            cell_ref = XLSX.utils.encode_cell({c: 5, r: R+1});
            ws[cell_ref] = cell;

            cell = {v: data[R].media_name, t: 's'};
            cell_ref = XLSX.utils.encode_cell({c: 6, r: R+1});
            ws[cell_ref] = cell;

            cell = {v: data[R].page, t: 's'};
            cell_ref = XLSX.utils.encode_cell({c: 7, r: R+1});
            ws[cell_ref] = cell;

            cell = {v: data[R].file_pdf, f: '=HYPERLINK("'+ data[R].file_pdf +'","Link")'};
            cell_ref = XLSX.utils.encode_cell({c: 8, r: R+1});
            ws[cell_ref] = cell;

            cell = {v: data[R].tone, t: 's'};
            cell_ref = XLSX.utils.encode_cell({c: 9, r: R+1});
            ws[cell_ref] = cell;

            cell = {v: data[R].journalist, t: 's'};
            cell_ref = XLSX.utils.encode_cell({c: 10, r: R+1});
            ws[cell_ref] = cell;

            cell = {v: data[R].prvalue, t: 's'};
            cell_ref = XLSX.utils.encode_cell({c: 11, r: R+1});
            ws[cell_ref] = cell;

            cell = {v: data[R].advalue, t: 's'};
            cell_ref = XLSX.utils.encode_cell({c: 12, r: R+1});
            ws[cell_ref] = cell;
          }
          ws['!cols'] = [
            {wch:3}, // "characters"
            {wpx:75}, // "pixels"
            {wch:12},
            {wpx:100},
            {wch:24},
            {wpx:125},
            {wch:24},
            {wpx:125},
            {wch:24},
            {wpx:125},
            {wch:24},
            {wpx:125},
            {wch:12},
            {wpx:100},
            {wch:12},
            {wpx:100}
          ];
          if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
          return ws;
        }

        function s2ab(s) {
          var buf = new ArrayBuffer(s.length);
          var view = new Uint8Array(buf);
          for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
          return buf;
        }



        vm.downloadPPT = function(){
          /* Home 1 */
          // area chart
          var pptItem = [];
          var mv = mediaVisibilityChartData;
          var mvjson = {};
          var colors = ['ff0000', '00ff00', '0000ff', 'ffff00', 'ff00ff', '00ffff', '000000', '90C3D4', 'D4A190', 'A1D490', 'C390D4', 'E39DDD', '9DE3BA', 'D4505E', 'D4CF50'];
          mvjson.title = 'Media Visibility';
          mvjson.renderType = 'column';
          mvjson.data = [];

          for(var i=0;i<mv.series.length;i++){
            mvjson.data.push({
              name: mv.series[i].name,
              labels: mv.categories,
              values: mv.series[i].data,
              color: colors[i]
            });
          }

          pptItem.push(mvjson);

          // pie chart (summary)
          var sc = summaryChartData;
          var scjson = {};
          var labels = [],
              values = [];
          scjson.title = 'Summary';
          scjson.renderType = 'pie';
          scjson.data = [];

          for(var i=0;i<sc.length;i++){
            if(sc[i][1] > 0){
              labels.push(sc[i][0]);
              values.push(sc[i][1]);
            }
          }
          scjson.data.push({
            name: 'Summary',
            labels: labels,
            values: values,
            colors: colors
          });

          pptItem.push(scjson);
          /* Home 1 */

          /* Home 2 */
          var labelsarr = ['Positive', 'Neutral', 'Negative'];

          // Coverage Tonality
          var ct = coverageTonalityChartData;
          var ctjson = {};
          ctjson.title = 'Coverage Tonality';
          ctjson.renderType = 'bar';
          ctjson.data = [];

          for(var i=0;i<ct.category.length;i++){
            var category = ct.category[i];
            var tempjson = {};
            var posvalue = 0,
                neuvalue = 0,
                negvalue = 0;
            tempjson.name = category;

            for(var j=0;j<ct.series.length;j++){
              switch(ct.series[j].name){
                case labelsarr[0]:
                  posvalue = ct.series[j].data[i];
                  break;
                case labelsarr[1]:
                  neuvalue = ct.series[j].data[i];
                  break;
                case labelsarr[2]:
                  negvalue = ct.series[j].data[i];
                  break;
              }
            }

            tempjson.labels = labelsarr;
            tempjson.values = [];
            tempjson.values.push(posvalue);
            tempjson.values.push(neuvalue);
            tempjson.values.push(negvalue);

            ctjson.data.push(tempjson);
          }

          pptItem.push(ctjson);

          // Tone by Category chart
          var tc = toneByCategoryChartData;
          var tcjson = {};
          tcjson.title = 'Tone by Category';
          tcjson.renderType = 'bar';
          tcjson.data = [];

          for(var i=0;i<tc.category.length;i++){
            var category = tc.category[i];
            var tempjson = {};
            var posvalue = 0,
                neuvalue = 0,
                negvalue = 0;
            tempjson.name = category;

            for(var j=0;j<tc.series.length;j++){
              switch(tc.series[j].name){
                case labelsarr[0]:
                  posvalue = tc.series[j].data[i];
                  break;
                case labelsarr[1]:
                  neuvalue = tc.series[j].data[i];
                  break;
                case labelsarr[2]:
                  negvalue = tc.series[j].data[i];
                  break;
              }
            }

            tempjson.labels = labelsarr;
            tempjson.values = [];
            tempjson.values.push(posvalue);
            tempjson.values.push(neuvalue);
            tempjson.values.push(negvalue);

            tcjson.data.push(tempjson);
          }

          pptItem.push(tcjson);

          // Tone by Media Selection
          var ms = mediaSelectionChartData;
          var msjson = {};
          msjson.title = 'Tone by Media Selection';
          msjson.renderType = 'bar';
          msjson.data = [];

          for(var i=0;i<ms.category.length;i++){
            var category = ms.category[i];
            var tempjson = {};
            var posvalue = 0,
                neuvalue = 0,
                negvalue = 0;
            tempjson.name = category;

            for(var j=0;j<ms.series.length;j++){
              switch(ms.series[j].name){
                case labelsarr[0]:
                  posvalue = ms.series[j].data[i];
                  break;
                case labelsarr[1]:
                  neuvalue = ms.series[j].data[i];
                  break;
                case labelsarr[2]:
                  negvalue = ms.series[j].data[i];
                  break;
              }
            }

            tempjson.labels = labelsarr;
            tempjson.values = [];
            tempjson.values.push(posvalue);
            tempjson.values.push(neuvalue);
            tempjson.values.push(negvalue);

            msjson.data.push(tempjson);
          }

          pptItem.push(msjson);
          /* Home 2 */

          /* Home 3 */
          var ews = EWSChartData;
          var ewsjson = {};
          var colors = ['ff0000', '00ff00', '0000ff', 'ffff00', 'ff00ff', '00ffff', '000000', '90C3D4', 'D4A190', 'A1D490', 'C390D4', 'E39DDD', '9DE3BA', 'D4505E', 'D4CF50'];
          ewsjson.title = 'Early Warning System';
          ewsjson.renderType = 'column';
          ewsjson.data = [];

          for(var i=0;i<ews.series.length;i++){
            ewsjson.data.push({
              name: ews.series[i].name,
              labels: ews.categories,
              values: ews.series[i].data,
              color: colors[i]
            });
          }

          pptItem.push(ewsjson);
          /* Home 3 */

          var items = {data: pptItem};
          var filename = "charts - " + new Date() + ".pptx";
          $http({
            method: 'POST',
            url: '/api/dashboards/downloadppt',
            data: items,
            responseType: "blob"
          }).then(function(response){
            FileSaver.saveAs(response.data, filename);
          });
        };
    }
})();
