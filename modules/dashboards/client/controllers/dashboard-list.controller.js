/**=========================================================
 * Module: flot-chart.js
 * Setup options and data for flot chart directive
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.dashboards')
        .controller('DashboardsListArticleController', DashboardsListArticleController);

    // DashboardsListArticleController.$inject = ['$scope', '$state', 'Authentication', 'ChartData', '$timeout', 'Colors', '$q', '$rootScope', 'ArticleListData', '$sce', '$http', '$modalInstance', '$filter', 'newsDataService', '$window', 'ngTableParams', 'items'];
    // function DashboardsListArticleController($scope, $state, Authentication, ChartData, $timeout, Colors, $q, $rootScope, ArticleListData, $sce, $http, $modalInstance, $filter, newsDataService, $window, ngTableParams, items) {
    DashboardsListArticleController.$inject = ['$scope', '$state', 'Authentication', 'ChartData', '$timeout', 'Colors', '$q', '$rootScope', 'ArticleListData', '$sce', '$http', '$uibModalInstance', '$filter', 'newsDataService', '$window', 'ngTableParams', 'items'];
    function DashboardsListArticleController($scope, $state, Authentication, ChartData, $timeout, Colors, $q, $rootScope, ArticleListData, $sce, $http, $uibModalInstance, $filter, newsDataService, $window, ngTableParams, items) {
        var vm = this;

        if(Authentication.user === ""){
          return $state.go("page.authentication.signin");
        }

        $scope.authentication = Authentication;
        var deferred = $q.defer();
        var filterBucket = false;
        var td = [];

        // $scope.initData = ArticleListData.getObject();
        $scope.initData = items;
        // Pagination
        $scope.currentPage = 1;
        $scope.maxSize = 5;

        function populateList(val){
          var params = val;
          // params.time_frame = $rootScope.selectedPeriodOptions.value;

          if(val.type === 'ews'){
            $http.post('/api/dashboards/getarticlelist', params.old_url).success(function(response) {
              $scope.articleLists = response.data;

              $scope.totalItems = response.pagination.total_articles;
            }).error(function(response) {
              $scope.articleLists = null;
            });
          } else{
            $scope.tableParams = new ngTableParams({
                page: 1,
                count: 25,
                sorting: {
                    datee: 'desc'
                }
            }, {
                getData: function($defer, params){

                    if(!checkFilterChanging(filterBucket, params) && td.length > 0){
                        td = params.sorting() ? $filter('orderBy')(td, params.orderBy()) : td;
                        var searchedData = searchData();
                        params.total(searchedData.length);
                        $defer.resolve(searchedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    } else{
                        return newsDataService.postToServer('/api/dashboards/getarticlelistfromcharts', val, function(response){
                            filterBucket = params;
                            var results = response.data;
                            td = results.data;
                            td = params.sorting() ? $filter('orderBy')(td, params.orderBy()) : td;
                            var searchedData = searchData();
                            console.log('banyak: ' + searchedData.length);
                            params.total(searchedData.length); 

                            $defer.resolve(searchedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                        });
                    }

                }
            });
          }
        };

        $scope.trustAsHtml = $sce.trustAsHtml;

        $scope.requestData = function(val) {
          $scope.requestParameters = val;
          $scope.requestParameters.offset = 0;
          populateList(val);
        };

        $scope.getDetailArticle = function(val) {
          var url = '/api/dashboards/getarticledetail?_id=' + val;
          $scope.mediaHTML = "";
          $http.get(url).success(function(response) {

            var results = response.data.data;

            var keywords = results.keywords;
            var selectedKeyword;

            var temp;
            var x, y;
            for(var j=0;j<keywords.length;j++){
              selectedKeyword = keywords[j].match(/\w+|"[^"]+"/g);
              for(var k=0;k<selectedKeyword.length;k++){
                x = selectedKeyword[k];
                x = x.replace(/\"/g, "");
                x = x.replace(/\'/g, "");

                temp = results.content.replace(new RegExp(x, "ig"), "<span style='color:#f89406; font-weight:bolder;'>" + x + "</span>");
                results.content = temp;
              }
            }

            results.content = results.content.replace(/(?:\r\n|\r|\n)/g, '<br />');
            
            if(results.file_pdf.indexOf('.mp4') !== -1){
              var media_type = (results.file_pdf.indexOf('.mp3') !== -1) ? 'media_radio' : 'media_tv';
              var file = response.data.data.file_pdf;
              var tanggal = file.substring(8, 10),
              bulan = file.substring(5, 7),
              tahun = file.substring(0, 4);

              $scope.mediaHTML = '<video width="25%" controls>' +
                '<source src="' + 'http://pdf.antara-insight.id/media_tv/' + tahun + '/' + bulan + '/' + tanggal + '/' + file + '" type="video/mp4">' +
                'Your browser does not support the video tag.' +
              '</video>';

            } else if(results.file_pdf.indexOf('.mp3') !== -1){
              var file = response.data.data.file_pdf;
              var tanggal = file.substring(8, 10),
              bulan = file.substring(5, 7),
              tahun = file.substring(0, 4);

              $scope.mediaHTML = '<audio width="25%" controls>' +
                '<source src="' + 'http://pdf.antara-insight.id/media_radio/' + tahun + '/' + bulan + '/' + tanggal + '/' + file + '" type="audio/mpeg">' +
                'Your browser does not support the audio tag.' +
              '</audio>';
            } else{
              results.file_pdf = results.file_pdf;
            }

            $scope.articleDetail = results;

            $.fancybox.open({href: '#dd1'});

          }).error(function(response) {
            $scope.articleDetail = null;
          });
        };

        // Page changing
        $scope.pageChanged = function() {
          $scope.requestParameters.offset = ($scope.currentPage - 1) * 10;
          populateList($scope.requestParameters);
        };

        $scope.goBackToPreviousState = function(){
          $state.go($rootScope.previousState.name);
        };


        $scope.ok = function () {
          // $modalInstance.close($scope.selected.item);
          $uibModalInstance.close();
        };

        $scope.cancel = function () {
          $uibModalInstance.dismiss('cancel');
        };

        $scope.$watch(function(){
          return $scope.searchTable;
        }, function () {
          if($scope.tableParams)
            $scope.tableParams.reload();
        });

        function searchData(){
          if($scope.searchTable)
             return $filter('filter')(td,$scope.searchTable);
          return td;
        }

        $scope.checkExpanded = function(article){
          if(article.expanded) article.expanded = false;
          else article.expanded = true;
        }

        $scope.saveActivity = function(article, type){
          var items = [{
            article_id: article.article_id,
            category_id: article.category_id,
            activity: type
          }];

          newsDataService.postToServer('/api/news/saveactivity', items, function(response){
            return;
          });
        };

        $scope.getPDF = function(type, article){
          var items = {
            type: type,
            article_id: article.detail.article_id,
            article_detail: article.detail
          };

          newsDataService.postToServer('/api/news/generatepdf', items, function(response){
            var fn = 'pdf_' + Date.now().toString() + '.pdf';
            pdfMake.createPdf(response.data).download(fn);
          });
          
        };

        $scope.originaleSingle = function(stat, formate, article, type){
          var items = [{
            article_id: article.article_id,
            category_id: article.category_id,
            activity: type
          }];
          

          newsDataService.postToServer('/api/news/saveactivity', items, function(response){
            var url = 'http://pdf.antara-insight.id?stat=' + stat;
            url += '&article=' + article.article_id;
            url += '&formate=' + formate;
            url += '&logo_head=' + Authentication.user.user_detail.logo_head;
            url += '&client_id=' + Authentication.user.user_detail.client_id;

            $window.open(url, '_blank');
          });

        };

        function checkFilterChanging(ofo, nfo){
          // if( ofo.gc === nfo.gc && ofo.sc === nfo.sc && ofo.ms === nfo.ms && ofo.mi === nfo.mi && ofo.tf === nfo.tf && ofo.df === nfo.df && ofo.dt === nfo.dt ){
          //   return false;
          // } else{
          //   return true;
          // }
          return false;
        }
    }
})();
