/**=========================================================
 * Module: NGTableCtrl.js
 * Controller for ngTables
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.editings')
        .controller('EditingsModalController', ModalCtrl);
    /*jshint -W055 */
    ModalCtrl.$inject = ['Authentication', '$scope', '$uibModalInstance', 'editingsDataService', 'items'];
    function ModalCtrl(Authentication, $scope, $uibModalInstance, editingsDataService, items) {

        $scope.items = items;
        // $scope.selected = {
        //   item: $scope.items[0]
        // };

        $scope.ok = function () {
          $uibModalInstance.close($scope.selected.item);
        };

        $scope.cancel = function () {
          $uibModalInstance.dismiss('cancel');
        };

        $scope.doUpdateSummary = function(){
          $scope.items.selectedArticleData.activity = 'summary_edited';

          editingsDataService.postToServer('/api/editings/editsummary', $scope.items.selectedArticleData, function(response){
            $uibModalInstance.close($scope.items.selectedArticleData);
          });
        };

        $scope.doUpdateTonality = function(){
          $scope.items.selectedArticleData.activity = 'tone_edited';

          editingsDataService.postToServer('/api/editings/editdataclient', $scope.items.selectedArticleData, function(response){
            $uibModalInstance.close($scope.items.selectedArticleData);
          });
        };

    }
})();


