/**=========================================================
 * Module: NGTableCtrl.js
 * Controller for ngTables
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.editings')
        .controller('EditingsController', NGTableCtrl);
    /*jshint -W055 */
    NGTableCtrl.$inject = ['Authentication', '$filter', 'ngTableParams', '$resource', '$timeout', 'editingsDataService', '$rootScope', '$http', '$scope', '$sce', '$q', '$uibModal', '$log'];
    function NGTableCtrl(Authentication, $filter, ngTableParams, $resource, $timeout, editingsDataService, $rootScope, $http, $scope, $sce, $q, $uibModal, $log) {
        var vm = this;
        var deferred = $q.defer();
        var filterBucket = false;
        var td = [];
        vm.selection = [];
        vm.authentication = Authentication;
        vm.title = 'Controller';
        vm.tableParams;
        vm.selectedAll = false;
        vm.totalArticles = 0;

        // Call getTableData()
        vm.initData = function(){
          $timeout(function(){
            vm.tableParams = [];
            vm.loadingData = true;
            getTableData();
          }, 5000);
        };

        vm.reloadTables = function(){
            td = [];
            vm.tableParams = [];
            getTableData();
        };

        vm.trustAsHtml = $sce.trustAsHtml;

        ////////////////

        function activate() {
          var data = [
              {name: 'Moroni',  age: 50, money: -10   },
              {name: 'Tiancum', age: 43, money: 120   },
              {name: 'Jacob',   age: 27, money: 5.5   },
              {name: 'Nephi',   age: 29, money: -54   },
              {name: 'Enos',    age: 34, money: 110   },
              {name: 'Tiancum', age: 43, money: 1000  },
              {name: 'Jacob',   age: 27, money: -201  },
              {name: 'Nephi',   age: 29, money: 100   },
              {name: 'Enos',    age: 34, money: -52.5 },
              {name: 'Tiancum', age: 43, money: 52.1  },
              {name: 'Jacob',   age: 27, money: 110   },
              {name: 'Nephi',   age: 29, money: -55   },
              {name: 'Enos',    age: 34, money: 551   },
              {name: 'Tiancum', age: 43, money: -1410 },
              {name: 'Jacob',   age: 27, money: 410   },
              {name: 'Nephi',   age: 29, money: 100   },
              {name: 'Enos',    age: 34, money: -100  }
          ];

          // SORTING
          // ----------------------------------- 

          vm.data = data;

          vm.tableParams = new ngTableParams({
              page: 1,            // show first page
              count: 10,          // count per page
              sorting: {
                  name: 'asc'     // initial sorting
              }
          }, {
              total: data.length, // length of data
              getData: function($defer, params) {
                  // use build-in angular filter
                  var orderedData = params.sorting() ?
                          $filter('orderBy')(data, params.orderBy()) :
                          data;
          
                  $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
              }
          });

        }


        function getTableData() {
          vm.tableParams = new ngTableParams({
            page: 1,
            count: 25,
            sorting: {
              datee: 'desc'
            }
          }, {
            getData: function($defer, params){

              if(!checkFilterChanging(filterBucket, vm.filterOptions) && td.length > 0){
                  td = params.sorting() ? $filter('orderBy')(td, params.orderBy()) : td;
                  var searchedData = searchData();
                  params.total(searchedData.length);
                  $defer.resolve(searchedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
              } else{
                return editingsDataService.postToServer('/api/editings/gettablelist', vm.filterOptions, function(response){
                  filterBucket = vm.filterOptions;
                  var results = response.data;
                  td = results.data;
                  td = params.sorting() ? $filter('orderBy')(td, params.orderBy()) : td;
                  var searchedData = searchData();
                  params.total(searchedData.length); 

                  $defer.resolve(searchedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                });
              }

            }
          });

          // return;
        }


        vm.checkSelectionExists = function(article){
          var found = false;
          for(var i=0;i<vm.selection.length;i++) {
            if(vm.selection[i].article_id.toString() === article.article_id.toString() && (vm.selection[i].category_id.length == article.categories.length && vm.selection[i].category_id.every(function(u, i) {return u === article.categories[i]})) && (vm.selection[i]._id.length == article._id.length && vm.selection[i]._id.every(function(u, i) {return u === article._id[i]})) ){
              found = true;
              break;
            }
          }

          return found;
        };

        vm.toggleSelection = function toggleSelection(article) {
          var found = false;
          var index = null;
          
          if(vm.selection.length > 0){
            for(var i=0;i<vm.selection.length;i++) {
              if(vm.selection[i].article_id.toString() === article.article_id.toString() && (vm.selection[i].category_id.length == article.categories.length && vm.selection[i].category_id.every(function(u, i) {return u === article.categories[i]})) && (vm.selection[i]._id.length == article._id.length && vm.selection[i]._id.every(function(u, i) {return u === article._id[i]})) ){
                  index = i;
                  break;
              }
            }
          }

          // is currently selected
          if(index !== null){
            vm.selection.splice(index, 1);
          // is newly selected
          } else{
            vm.selection.push({
              _id: article._id,
              article_id: article.article_id,
              category_id: article.categories
            });
          }

          if(vm.selection.length > 0){
            vm.showButtonEmail = true;
          } else{
            vm.showButtonEmail = false;
          }
        };



        // vm.getDetailArticle = function(val, mn, categories) {
        //   var url = '/api/dashboards/getarticledetail?_id=' + val;
        //   vm.mediaHTML = "";
        //   $http.get(url).success(function(response) {
            
        //     var results = response.data.data;

        //     var keywords = results.keywords;
        //     var selectedKeyword;

        //     var temp;
        //     var x, y;
        //     for(var j=0;j<keywords.length;j++){
        //       selectedKeyword = keywords[j].match(/\w+|"[^"]+"/g);
        //       for(var k=0;k<selectedKeyword.length;k++){
        //         x = selectedKeyword[k];
        //         x = x.replace(/\"/g, "");
        //         x = x.replace(/\'/g, "");

        //         temp = results.content.replace(new RegExp('('+x+')', "ig"), "<span style='color:#f89406; font-weight:bolder;'>" + x + "</span>");
        //         results.content = temp;
        //       }
        //     }

        //     results.content = results.content.replace(/(?:\r\n|\r|\n)/g, '<br />');
            
        //     if(results.file_pdf.indexOf('.mp4') !== -1){
        //       var media_type = (results.file_pdf.indexOf('.mp3') !== -1) ? 'media_radio' : 'media_tv';
        //       var file = response.data.data.file_pdf;
        //       var tanggal = file.substring(8, 10),
        //       bulan = file.substring(5, 7),
        //       tahun = file.substring(0, 4);

        //       vm.mediaHTML = '<video width="25%" controls>' +
        //         '<source src="' + 'http://pdf.antara-insight.id/media_tv/' + tahun + '/' + bulan + '/' + tanggal + '/' + file + '" type="video/mp4">' +
        //         'Your browser does not support the video tag.' +
        //       '</video>';

        //     } else if(results.file_pdf.indexOf('.mp3') !== -1){
        //       var file = response.data.data.file_pdf;
        //       var tanggal = file.substring(8, 10),
        //       bulan = file.substring(5, 7),
        //       tahun = file.substring(0, 4);

        //       vm.mediaHTML = '<audio width="25%" controls>' +
        //         '<source src="' + 'http://pdf.antara-insight.id/media_radio/' + tahun + '/' + bulan + '/' + tanggal + '/' + file + '" type="audio/mpeg">' +
        //         'Your browser does not support the audio tag.' +
        //       '</audio>';
        //     } else{
        //       results.file_pdf = results.file_pdf;
        //     }

        //     vm.articleDetail = results;
        //     vm.articleDetail.media_name = mn;
        //     vm.articleDetail.selectedCategories = categories;

        //     // $.fancybox.open({href: '#dd1'});
        //   }).error(function(response) {
        //     vm.articleDetail = null;
        //   });
        // };



        vm.doUpdateSummary = function(){
          vm.selectedArticleData.activity = 'summary_edited';
          $http.post('/api/summary/editsummary', vm.selectedArticleData)
            .success(function (response) {
              // $.fancybox.close();
            })
            .error(function (errResponse) {
              alert('Failed to edit');
            });
        };


        vm.doUpdateTonality = function(){
          vm.selectedArticleData.activity = 'tone_edited';
          // return console.log(JSON.stringify($scope.selectedArticleData));
          $http.post('/api/summary/editdataclient', vm.selectedArticleData)
            .success(function (response) {
              // $.fancybox.close();
            })
            .error(function (errResponse) {
              alert(JSON.stringify(errResponse));
            });
        };

        $scope.$watch(function(){
          return vm.searchTable;
        }, function () {
          if(vm.tableParams)
            vm.tableParams.reload();
        });

        function searchData(){
          if(vm.searchTable)
             return $filter('filter')(td,vm.searchTable);
          return td;
        }

        function checkFilterChanging(ofo, nfo){
          if( ofo.gc === nfo.gc && ofo.sc === nfo.sc && ofo.ms === nfo.ms && ofo.mi === nfo.mi && ofo.tf === nfo.tf && ofo.df === nfo.df && ofo.dt === nfo.dt ){
            return false;
          } else{
            return true;
          }
        }

        vm.highlightKeyword = function(article){
          var temp = article.detail.content;
          var keywords = article.keywords;
          var selectedKeyword;
          var x, y;
          for(var j=0;j<keywords.length;j++){
            selectedKeyword = keywords[j].match(/\w+|"[^"]+"/g);
            for(var k=0;k<selectedKeyword.length;k++){
              x = selectedKeyword[k];
              x = x.replace(/\"/g, "");
              x = x.replace(/\'/g, "");

              temp = temp.replace(new RegExp('('+x+')', "ig"), "<span style='color:#f89406; font-weight:bolder;'>$1</span>");
            }
          }

          return temp.replace(/(?:\r\n|\r|\n)/g, '<br />');
        }

        vm.checkExpanded = function(article){

          if(article.expanded) article.expanded = false;
          else article.expanded = true;
        }

        vm.checkAll = function(){
          if(!vm.selectedAll) {
            vm.selectedAll = true;
            angular.forEach(td, function (value, key) {
              if(!vm.checkSelectionExists(value)){
                vm.selection.push({
                  _id: value._id,
                  article_id: value.article_id,
                  category_id: value.categories
                });
              }
            });
          } else {
            vm.selectedAll = false;
            // for(var j=0;j<$scope.tableData.length;j++){
            //   for(var i=0;i<$scope.selection.length;i++){
            //     if($scope.tableData[j].article_id.toString() === $scope.selection[i].article_id.toString()){
            //       $scope.selection.splice(i, 1);
            //       break;
            //     }
            //   }
            // }
            vm.selection = [];
          }
        };

        vm.editSummary = function(val, dest) {
          vm.updateValue = val.tone;
          vm.selectedArticleData = val;
          $.fancybox.open({href: '#' + dest});
        };


        vm.items = ['item1', 'item2', 'item3'];
        vm.open = function (items) {

          var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'editingsModal.html',
            controller: 'EditingsModalController',
            // size: size,
            resolve: {
              items: function () {
                // return vm.items;
                return items;
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            vm.selected = selectedItem;
          }, function () {
            $log.info('Modal dismissed at: ' + new Date());
          });
        };


    }
})();


