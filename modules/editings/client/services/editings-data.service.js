(function() {
    'use strict';

    angular
        .module('app.editings')
        .service('editingsDataService', ngTableDataService);


    ngTableDataService.$inject = ['$http'];
    function ngTableDataService($http) {
        /* jshint validthis:true */
        var self = this;
        this.cache = null;
        this.getData = getData;

        this.postToServer = function(url, datas, success){
          // console.log(JSON.stringify(datas));
          var httpRes = $http.post(url, datas).then(function(response) {
              success(response);
          });
        };

        ////////////////

        function getData($defer, params, api) {
          // if no cache, request data and filter
          if ( ! self.cache ) {
            if ( api ) {
              api.get(function(data){
                self.cache = data;
                filterdata($defer, params);
              });
            }
          }
          else {
            filterdata($defer, params);
          }
          
          function filterdata($defer, params) {
            var from = (params.page() - 1) * params.count();
            var to = params.page() * params.count();
            var filteredData = self.cache.result.slice(from, to);

            params.total(self.cache.total);
            $defer.resolve(filteredData);
          }

        }
    }
})();
