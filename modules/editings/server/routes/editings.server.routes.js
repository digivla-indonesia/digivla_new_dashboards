'use strict';

var editingsPolicy = require('../policies/editings.server.policy'),
  editings = require('../controllers/editings.server.controller');

module.exports = function(app) {

  app.route('/api/editings/gettablelist')
    .post(editings.getTableList);

  app.route('/api/editings/editsummary')
  	.post(editings.updateSummary);

  app.route('/api/editings/editdataclient')
  	.post(editings.updateDataClient);

  app.route('/api/editings/deletearticle')
    .post(editings.deleteArticles);

  app.route('/api/editings/getcategorylist')
    .get(editings.getCategoryList);

  app.route('/api/editings/savearticle')
    .post(editings.saveArticle);

  /** these route is not used for get article detail
  app.route('/api/dashboard/getdetailarticle').all(summaryPolicy.isAllowed)
     .post(summary.getArticleDetail);
  */
};