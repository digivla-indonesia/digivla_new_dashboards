(function() {
    'use strict';

    angular
        .module('app.extras')
        .config(appRoutes);
    appRoutes.$inject = ['$stateProvider', 'RouteHelpersProvider'];

    function appRoutes($stateProvider, helper) {

        $stateProvider
            .state('app.configurations', {
                url: '/settings',
                title: 'Settings',
                templateUrl: 'modules/extras/client/views/settings.client.view.html',
                resolve: helper.resolveFor('filestyle')
            })
            ;

    }
})();