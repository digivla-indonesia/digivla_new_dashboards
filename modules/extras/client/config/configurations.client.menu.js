(function() {
    'use strict';

    angular
        .module('app.extras')
        .run(coreMenu);

    coreMenu.$inject = ['Menus'];
    function coreMenu(Menus){
        Menus.addMenuItem('sidebar', {
            title: 'Configuration',
            state: 'app.configurations',
            iconClass: 'icon-user',
            position: 1,
            roles: ['*']
        });
    }

})();