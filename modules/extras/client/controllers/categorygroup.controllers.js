
(function() {
    'use strict';

    angular
        .module('app.extras')
        .controller('CategoryGroupController', TodoController);

    TodoController.$inject = ['$filter', '$http', '$uibModal', '$log'];
    function TodoController($filter, $http, $uibModal, $log) {
        var vm = this;

        
        function initData(){
			$http.get('/api/users/getassets?t=groupcategory').success(function (response) {
				vm.dataSet = response.data;
				vm.dataSetError = false;
			}).error(function (response) {
				vm.dataSetError = true;
			});
		};

		function getDetailData(cs){
			vm.chosenGroupCategory = cs.category_set;
			vm.dataDetail = [];
			vm.showDetail = true;
			vm.clisetName = cs.descriptionz;
			var url = '/api/configurations/categorysetdetail?group_category_id=' + cs.category_set;
			$http.get(url).success(function (response) {
				// $scope.dataDetail = response.data;
				// console.log(JSON.stringify(response));
				vm.dataDetail = response;
				vm.dataDetailError = false;
			}).error(function (response) {
				vm.dataDetailError = true;
			});
		};

		vm.initSetData = function(){
			initData();
		};

		vm.showDetail = false;

		vm.getDetail = function(cs){
			getDetailData(cs);
		};

		// edit
		vm.openEditCliset = function(val){
			vm.clisetValue = val.descriptionz;
			vm.clisetId = val.category_set;
		};

		vm.editClisetName = function(){
			//alert('clisetValue: ' + $scope.clisetValue + ', clisetId: ' + $scope.clisetId);
			var url = '/api/users/getassets?t=groupcategory';
			var items = {
				_id: vm.clisetId,
				name: vm.clisetValue
			};

			$http.put(url, items).success(function(resp){
				initData();
				// $rootScope.$broadcast('reloadFilterOption');

				// when submit is done
				vm.clisetValue = '';
				vm.clisetId = null;
				$.fancybox.close();
			}).error(function(resp){
				alert('Failed to update');
			});
		};

		// update category
		vm.updateCategoryList = function(){
			var uri = '/api/configurations/categorysetdetail';
			var uriItem = {
				category_set: vm.chosenGroupCategory,
				list: vm.dataDetail
			};
			$http.put(uri, uriItem).success(function(resp){
				// alert("success");
				getDetailData({category_set: vm.chosenGroupCategory, descriptionz: vm.clisetName});
				// $rootScope.$broadcast('reloadFilterOption');
				// $scope.keyword = '';
				// $scope.dateFrom = '';
				// $scope.dateTo = '';
			}).error(function(resp){
				alert('Error adding keyword');
			});
		};

		// new category set
		vm.newCategorySet = '';
		vm.addNewCategorySet = function(){
			if(vm.newCategorySet == ''){
				return alert('You have to fill category set name');
			} else{
				var url = '/api/configurations/tb_category_set_type';
				var items = {
					descriptionz: vm.newCategorySet
				};
				$http.post(url, items).success(function(resp){
					initData();
					// $rootScope.$broadcast('reloadFilterOption');
					vm.newCategorySet = '';
				}).error(function(resp){
					alert('Failed to add category set');
				});
			}
		};

		// delete category set
		vm.getDeleteCategorySetId = function(val){
			vm.deleteCategorySetId = val;
		};

		vm.deleteCategorySet = function(){
			var url = '/api/configurations/tb_category_set_type?_id=' + vm.deleteCategorySetId;
			var items = {
				category_id_str: vm.deleteCategorySetId
			};
			$http.delete(url, items).success(function(resp){
				initData();
				// $rootScope.$broadcast('reloadFilterOption');
				vm.deleteCategorySetId = null; 
			}).error(function(resp){
				alert('Failed to delete sub category');
			});
		};

		vm.open = function (items) {

          var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'settingsModal.html',
            controller: 'ExtrasGroupMediaController',
            // size: size,
            resolve: {
              items: function () {
                // return vm.items;
                return items;
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            vm.selected = selectedItem;
          }, function () {
            $log.info('Modal dismissed at: ' + new Date());
          });
        };

    }
})();
