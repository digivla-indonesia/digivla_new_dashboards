
(function() {
    'use strict';

    angular
        .module('app.extras')
        .controller('SubCategoryController', TodoController);

    TodoController.$inject = ['$filter', '$http', '$uibModal', '$log'];
    function TodoController($filter, $http, $uibModal, $log) {
        var vm = this;

        
		function initData(){
			$http.get('/api/users/getsubassets?t=groupcategory').success(function (response) {
				vm.dataSet = response.data;
				vm.dataSetError = false;
			}).error(function (response) {
				vm.dataSetError = true;
			});
		};

		function getSubcliDetail(cs){
			vm.chosenSubCategory = cs.category_id;
			vm.dataDetail = [];
			vm.showDetail = true;
			vm.clisetName = cs.descriptionz;
			var url = '/api/configurations/subcategorysetdetail?category_id=' + cs.category_id;

			$http.get(url).success(function (response) {
				vm.dataDetail = response;
				vm.dataDetailError = false;
			}).error(function (response) {
				vm.dataDetailError = true;
			});
		};

		vm.initSetData = function(){
			initData();
		};

		vm.showDetail = false;

		vm.getDetail = function(cs){
			getSubcliDetail(cs);
			// $scope.chosenSubCategory = cs.category_id;
			// $scope.dataDetail = [];
			// $scope.showDetail = true;
			// $scope.clisetName = cs.descriptionz;
			// var url = '/api/configurations/subcategorysetdetail?category_id=' + cs.category_id;

			// $http.get(url).success(function (response) {
			//   $scope.dataDetail = response;
			//   $scope.dataDetailError = false;
			// }).error(function (response) {
			//   $scope.dataDetailError = true;
			// });
		};

		// Datepicker
		vm.clear = function () {
			vm.dateFrom = null;
			vm.dateTo = null;
		};

		vm.open1 = function($event) {
			$event.preventDefault();
			$event.stopPropagation();

			vm.opened1 = true;
			vm.opened = false;
		};

		vm.open = function($event) {
			$event.preventDefault();
			$event.stopPropagation();

			vm.opened = true;
			vm.opened1 = false;
		};


		vm.maxDate = new Date();
		vm.minDate = '2010-01-01';
		vm.format = 'yyyy-MM-dd';
		vm.dateFrom = '';
		vm.dateTo = '';


		// edit
		vm.openEditSubCliset = function(val){
			// alert(JSON.stringify(val));
			vm.subClisetValue = val.category_id;
			vm.subClisetId = val.category_id;
		};

		vm.editSubClisetName = function(){
			//alert('clisetValue: ' + $scope.clisetValue + ', clisetId: ' + $scope.clisetId);
			var url = '/api/users/getsubassets?t=groupcategory';
			var items = {
				_id: vm.subClisetId,
				name: vm.subClisetValue
			};

			$http.put(url, items).success(function(resp){
				initData();
				// $rootScope.$broadcast('reloadFilterOption');

				// when submit is done
				vm.subClisetValue = '';
				vm.subClisetId = null;
				$.fancybox.close();
			}).error(function(resp){
				alert('Failed to update');
			});
		};

		// add new sub category
		vm.newSubCategoryValue = '';
		vm.addNewSubCategory = function(){
			if(vm.newSubCategoryValue == ''){
				return alert('You have to fill sub category name');
			} else{
				var url = '/api/configurations/tb_category_list';
				var items = {
					category_id_str: vm.newSubCategoryValue
				};
				$http.post(url, items).success(function(resp){
					initData();
					// $rootScope.$broadcast('reloadFilterOption');
					vm.newSubCategoryValue = '';
				}).error(function(resp){
					alert('Failed to add sub category');
				});
			}
		};

		// delete sub category
		vm.getDeleteSubCategoryId = function(type, val){
			if(type == 0){
				vm.deleteType = type;
				vm.deleteSubCategoryId = val;
			} else if(type == 1){
				vm.deleteType = type;
				vm.deleteKeywordString = val;
			}
		};

		vm.deleteSubCategory = function(){
			if(vm.deleteType == 0){
				var url = '/api/configurations/tb_category_list?_id=' + vm.deleteSubCategoryId;
				var items = {
					category_id_str: vm.deleteSubCategoryId
				};
				$http.delete(url, items).success(function(resp){
					initData();
					// $rootScope.$broadcast('reloadFilterOption');
					vm.deleteSubCategoryId = null; 
				}).error(function(resp){
					alert('Failed to delete sub category');
				});
			} else if(vm.deleteType == 1){
				var url = '/api/configurations/keyword?category_id_str=' + vm.chosenSubCategory + '&keyword=' + encodeURIComponent(vm.deleteKeywordString);
				var items = {
					category_id_str: vm.chosenSubCategory,
					keyword: vm.deleteKeywordString
				};

				$http.delete(url, items).success(function(resp){
					// getSubcliDetail($scope.chosenSubCategory);
					getSubcliDetail({category_id: vm.chosenSubCategory, descriptionz: vm.clisetName});
					// $rootScope.$broadcast('reloadFilterOption');
					vm.deleteKeywordString = null; 
				}).error(function(resp){
					alert('Failed to delete sub category');
				});
			}
		};

		// insert category
		vm.keyword = '';

		vm.addNewKeyword = function(){
			var uri = '/api/configurations/keyword';
			var uriItem = {
				category_id: vm.chosenSubCategory,
				keyword: vm.keyword,
				date_from: vm.dateFrom,
				date_to: vm.dateTo
			};

			if(vm.dateFrom === '' || vm.dateTo === ''){
				return alert('Harap isi tanggal');
			}

			$http.post(uri, uriItem).success(function(resp){
				getSubcliDetail({category_id: vm.chosenSubCategory, descriptionz: vm.clisetName});
				vm.keyword = '';
				vm.dateFrom = '';
				vm.dateTo = '';
			}).error(function(resp){
				alert('Error adding keyword');
			});
		}

		vm.getDeleteKeyword = function(val){
			vm.deleteKeywordString = val;
		};

		vm.deleteKeyword = function(){
			alert(vm.chosenSubCategory);
			alert(vm.deleteKeywordString);
		};

		vm.open = function (items) {

          var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'settingsModal.html',
            controller: 'ExtrasGroupMediaController',
            // size: size,
            resolve: {
              items: function () {
                // return vm.items;
                return items;
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            vm.selected = selectedItem;
          }, function () {
            $log.info('Modal dismissed at: ' + new Date());
          });
        };

    }
})();
