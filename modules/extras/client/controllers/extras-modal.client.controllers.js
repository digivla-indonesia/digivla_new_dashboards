/**=========================================================
 * Module: NGTableCtrl.js
 * Controller for ngTables
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.extras')
        .controller('ExtrasGroupMediaController', ModalCtrl);
    /*jshint -W055 */
    ModalCtrl.$inject = ['Authentication', '$scope', '$uibModalInstance', 'extrasDataService', 'items'];
    function ModalCtrl(Authentication, $scope, $uibModalInstance, extrasDataService, items) {

        $scope.items = items;
        // $scope.selected = {
        //   item: $scope.items[0]
        // };

        $scope.ok = function () {
          $uibModalInstance.close($scope.selected.item);
        };

        $scope.cancel = function () {
          $uibModalInstance.dismiss('cancel');
        };


        $scope.doUpdateGroupMedia = function(){
          var url = '/api/users/getassets?t=media';
          var items = {
            _id: $scope.items.selectedArticleData.media_set_id,
            name: $scope.items.selectedArticleData.media_set_name
          };

          extrasDataService.putToServer(url, items, function(response){
            $uibModalInstance.close($scope.items.selectedArticleData);
          });
        };

        $scope.deleteMediaSet = function(){
          var url = '/api/configurations/tb_media_set?_id=' + $scope.selectedArticleData.media_set_id;
          var items = {
            media_set_id: $scope.selectedArticleData.media_set_id
          };

          extrasDataService.deleteToServer(url, items, function(response){
            $uibModalInstance.close($scope.items.selectedArticleData);
          });
        };
        
        $scope.editClisetName = function(){
          var url = '/api/users/getassets?t=groupcategory';
          var items = {
            _id: $scope.selectedArticleData.category_set,
            name: $scope.selectedArticleData.descriptionz
          };

          extrasDataService.putToServer(url, items, function(response){
            $uibModalInstance.close($scope.items.selectedArticleData);
          });
        };
        
        $scope.deleteCategorySet = function(){
          var url = '/api/configurations/tb_category_set_type?_id=' + $scope.selectedArticleData.category_set;
          var items = {
            category_id_str: $scope.selectedArticleData.category_set
          };

          extrasDataService.deleteToServer(url, items, function(response){
            $uibModalInstance.close($scope.items.selectedArticleData);
          });
        };
        
        $scope.editSubClisetName = function(){
          var url = '/api/users/getsubassets?t=groupcategory';
          var items = {
            _id: $scope.selectedArticleData.category_id,
            name: $scope.selectedArticleData.category_id
          };

          extrasDataService.putToServer(url, items, function(response){
            $uibModalInstance.close($scope.items.selectedArticleData);
          });
        };
        
        $scope.deleteSubCategory = function(){
          if($scope.items.deleteType == 0){
            var url = '/api/configurations/tb_category_list?_id=' + $scope.deleteSubCategoryId;
            var items = {
              category_id_str: $scope.selectedArticleData.category_id
            };

            extrasDataService.deleteToServer(url, items, function(response){
              $uibModalInstance.close($scope.items.selectedArticleData);
            });
          } else if($scope.items.deleteType == 1){
            var url = '/api/configurations/keyword?category_id_str=' + $scope.selectedArticleData.category_id + '&keyword=' + encodeURIComponent($scope.selectedArticleData.keyword);
            var items = {
              category_id_str: $scope.selectedArticleData.category_id
            };

            extrasDataService.deleteToServer(url, items, function(response){
              $uibModalInstance.close($scope.items.selectedArticleData);
            });
          }
        };

    }
})();


