
(function() {
    'use strict';

    angular
        .module('app.extras')
        .controller('MediaGroupController', TodoController);

    TodoController.$inject = ['$filter', '$http', '$uibModal', '$log'];
    function TodoController($filter, $http, $uibModal, $log) {
        var vm = this;

        
        function initData(){
			$http.get('/api/users/getassets?t=media').success(function (response) {
				//$scope.groupMedias = response.data;
				//$rootScope.selectedGroupMedia = $scope.groupMedias[0];
				vm.dataSet = response.data;
				vm.dataSetError = false;
			}).error(function (response) {
				//$scope.groupMedia = [{media_set_id: '', media_set_name: 'Choose Media Group'}];
				vm.dataSetError = true;
			});
		};

		vm.initSetData = function(){
			initData();
		};

		function getDetailData(ms){
			vm.chosenMediaSet = ms.media_set_id;
			vm.dataDetail = [];
			vm.showDetail = true;
			vm.mediaSetName = ms.media_set_name;
			var url = '/api/configurations/mediadetail?media_set=' + ms.media_set_id;
			$http.get(url).success(function (response) {
				vm.dataDetail = response;
				vm.dataDetailError = false;
			}).error(function (response) {
				vm.dataDetail = response;
				vm.dataDetailError = true;
			});
		};

		vm.getDetail = function(ms){
			// $scope.chosenMediaSet = ms.media_set_id;
			// $scope.dataDetail = [];
			// $scope.showDetail = true;
			// $scope.mediaSetName = ms.media_set_name;
			// var url = '/api/configurations/mediadetail?media_set=' + ms.media_set_id;
			// $http.get(url).success(function (response) {
			//   // $scope.dataDetail = response.data;
			//   $scope.dataDetail = response;
			//   $scope.dataDetailError = false;
			// }).error(function (response) {
			//   $scope.dataDetailError = true;
			// });
			getDetailData(ms);
		};

		// edit
		vm.openEditMedset = function(val){
			// alert(JSON.stringify(val));
			vm.medsetValue = val.media_set_name;
			vm.medsetId = val.media_set_id;
		};

		vm.editMedsetName = function(){
			//alert('clisetValue: ' + $scope.clisetValue + ', clisetId: ' + $scope.clisetId);
			var url = '/api/users/getassets?t=media';
			var items = {
				_id: vm.medsetId,
				name: vm.medsetValue
			};

			$http.put(url, items).success(function(resp){
				initData();
				// $rootScope.$broadcast('reloadFilterOption');
				// when submit is done
				vm.medsetValue = '';
				vm.medsetId = null;
				$.fancybox.close();
			}).error(function(resp){
				alert('Failed to update');
			});
		};

		// update category
		vm.updateMediaSetList = function(){
			var uri = '/api/configurations/mediadetail';
			var uriItem = {
				media_set_id: vm.chosenMediaSet,
				list: vm.dataDetail
			};

			// return alert(JSON.stringify($scope.dataDetail));

			$http.put(uri, uriItem).success(function(resp){
				getDetailData({media_set_id: vm.chosenMediaSet, media_set_name: vm.mediaSetName});
			}).error(function(resp){
				alert('Error adding keyword');
			});
		};

		// new media set
		vm.newMediaSet = '';

		vm.addNewMediaSet = function(){
			if(vm.newMediaSet == ''){
				return alert('You have to fill media set name');
			} else{
				var url = '/api/configurations/tb_media_set';
				var items = {
					media_name: vm.newMediaSet
				};
				$http.post(url, items).success(function(resp){
					initData();
					// $rootScope.$broadcast('reloadFilterOption');
					vm.newMediaSet = '';
				}).error(function(resp){
					alert('Failed to add category set');
				});
			}
		};

		// delete media set
		vm.getDeleteMediaSetId = function(val){
			vm.deleteMediaSetId = val;
		};

		vm.deleteMediaSet = function(){
			var url = '/api/configurations/tb_media_set?_id=' + vm.deleteMediaSetId;
			var items = {
				media_set_id: vm.deleteMediaSetId
			};
			$http.delete(url, items).success(function(resp){
				initData();
				// $rootScope.$broadcast('reloadFilterOption');
				vm.deleteMediaSetId = null; 
				vm.dataDetail = [];
				vm.showDetail = false;
			}).error(function(resp){
				alert('Failed to delete media');
			});
		};

		vm.selectAll = function () {
			angular.forEach(vm.dataDetail, function (obj) {
				angular.forEach(obj.media_list, function(obj){
					obj.chosen = true;
				});
			});
		};

		vm.clearAll = function () {
			angular.forEach(vm.dataDetail, function (obj) {
				angular.forEach(obj.media_list, function(obj){
					obj.chosen = false;
				});
			});
		};

		vm.open = function (items) {

          var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'settingsModal.html',
            controller: 'ExtrasGroupMediaController',
            // size: size,
            resolve: {
              items: function () {
                // return vm.items;
                return items;
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            vm.selected = selectedItem;
          }, function () {
            $log.info('Modal dismissed at: ' + new Date());
          });
        };

    }
})();
