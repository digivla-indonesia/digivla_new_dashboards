'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Configuration = mongoose.model('Configuration');

/**
 * Globals
 */
var user, configuration;

/**
 * Unit tests
 */
describe('Configuration Model Unit Tests:', function () {
  beforeEach(function (done) {
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: 'username',
      password: 'password'
    });

    user.save(function () {
      configuration = new Configuration({
        title: 'Configuration Title',
        content: 'Configuration Content',
        user: user
      });

      done();
    });
  });

  describe('Method Save', function () {
    it('should be able to save without problems', function (done) {
      return configuration.save(function (err) {
        should.not.exist(err);
        done();
      });
    });

    it('should be able to show an error when try to save without title', function (done) {
      configuration.title = '';

      return configuration.save(function (err) {
        should.exist(err);
        done();
      });
    });
  });

  afterEach(function (done) {
    Configuration.remove().exec(function () {
      User.remove().exec(done);
    });
  });
});
