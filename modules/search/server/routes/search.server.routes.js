'use strict';

var searchPolicy = require('../policies/search.server.policy'),
  search = require('../controllers/search.server.controller');

module.exports = function(app) {

  app.route('/api/search_article').all(searchPolicy.isAllowed)
    .get(search.searchArticle)
    .post(search.searchArticle);

  // app.route('/api/dashboard/getdetailarticle').all(searchPolicy.isAllowed)
  //   .post(search.detailArticle)
  //   .get(search.detailArticle);

  app.route('/api/search/generatepdf')
    .post(search.generatePDF);

  app.route('/api/search/generatedoc')
    .get(search.generateWord);

  app.route('/api/search/getcategorylist')
    .get(search.getCategoryList);

  app.route('/api/search/savearticle')
    .post(search.saveArticle);

  app.route('/api/search/getarticledetail')
    .get(search.detailArticle);
};