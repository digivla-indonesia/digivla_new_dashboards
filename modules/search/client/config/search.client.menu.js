(function() {
    'use strict';

    angular
        .module('app.search')
        .run(coreMenu);

    coreMenu.$inject = ['Menus'];
    function coreMenu(Menus){

        Menus.addMenuItem('sidebar', {
            title: 'Search',
            state: 'app.search',
            iconClass: 'icon-magnifier',
            position: 8,
            roles: ['*']
        });
        // Menus.addMenuItem('sidebar', {
        //     title: 'Editing',
        //     state: 'app.editing',
        //     type: 'dropdown',
        //     iconClass: 'icon-grid',
        //     position: 7,
        //     roles: ['*']
        // });

        // Menus.addSubMenuItem('sidebar', 'app.editing', {title: 'Standard',     state: 'app.editing-standard'});
        // Menus.addSubMenuItem('sidebar', 'app.editing', {title: 'Extended',     state: 'app.editing-extended'});
        // Menus.addSubMenuItem('sidebar', 'app.editing', {title: 'DataTables',   state: 'app.editing-datatable'});
        // Menus.addSubMenuItem('sidebar', 'app.editing', {title: 'ngTables',     state: 'app.editing-ngtable'});
        // Menus.addSubMenuItem('sidebar', 'app.editing', {title: 'uiGrid',       state: 'app.editing-uigrid'});
        // Menus.addSubMenuItem('sidebar', 'app.editing', {title: 'xEditable',    state: 'app.editing-xeditable'});
        // Menus.addSubMenuItem('sidebar', 'app.editing', {title: 'Angular Grid', state: 'app.editing-angulargrid'});

    }

})();