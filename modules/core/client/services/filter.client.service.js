'use strict';

//Menu service used for managing  menus
angular.module('core').service('FilterValues', [
  function () {
  	var filterValues = {};
  	// group category
  	this.groupCategory = function(val){
  		filterValues.groupCategory = val;
  		return val;
  	};
  	this.getGroupCategory = function(){
  		return this.groupCategory();
  	}

  	// sub category
  	this.subCategory = function(val){
  		filterValues.subCategory = val;
  		return val;
  	};
  	this.getSubCategory = this.subCategory;

  	// group media
  	this.groupMedia = function(val){
  		filterValues.groupMedia = val;
  		return val;
  	};
  	this.getGroupMeida = this.groupMedia;

  	// sub media
  	this.subMedia = function(val){
  		filterValues.subMedia = val;
  		return val;
  	};
  	this.getSubMedia = this.subMedia;

  	// period
  	this.period = function(val){
  		filterValues.period = val;
  		return val;
  	};
  	this.getPeriod = this.period;


  	this.getFilterValues = function(){
  		return filterValues;
  	}
  }

]);