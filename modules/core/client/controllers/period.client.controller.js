'use strict';

angular.module('core').controller('PeriodController', ['$scope', '$rootScope', '$state', '$http', 'Authentication',
  function($scope, $rootScope, $state, $http, Authentication) {
    // This provides Authentication context.
    $scope.authentication = Authentication;

    /* For option 'Pilih' */
    // setup clear
    // $scope.clear = function() {
    //   $scope.dateFrom = null;
    //   $scope.dateTo = null;
    // };

    // // open min-cal
    // $scope.open = function($event) {
    //   $event.preventDefault();
    //   $event.stopPropagation();

    //   $scope.opened = true;
    //   $scope.opened1 = false;
    // };

    // $scope.open1 = function($event) {
    //   $event.preventDefault();
    //   $event.stopPropagation();

    //   $scope.opened1 = true;
    //   $scope.opened = false;
    // };


    // $scope.dateOptions = {
    //   formatYear: 'yy',
    //   startingDay: 1
    // };

    $rootScope.dateFrom = '';
    $rootScope.dateTo = '';
    /* For option 'Pilih' */





    $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function() {
      $scope.dt = null;
    };

    $scope.inlineOptions = {
      customClass: getDayClass,
      minDate: new Date(),
      showWeeks: true
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      maxDate: new Date(),
      minDate: new Date(2010, 1, 1),
      startingDay: 1
    };

    // Disable weekend selection
    // function disabled(data) {
    //   var date = data.date,
    //     mode = data.mode;
    //   return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    // }

    $scope.toggleMin = function() {
      $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
      $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.open1 = function() {
      $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
      $scope.popup2.opened = true;
    };

    $scope.setDate = function(year, month, day) {
      $scope.dt = new Date(year, month, day);
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
      opened: false
    };

    $scope.popup2 = {
      opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date(tomorrow);
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
      {
        date: tomorrow,
        status: 'full'
      },
      {
        date: afterTomorrow,
        status: 'partially'
      }
    ];

    function getDayClass(data) {
      var date = data.date,
        mode = data.mode;
      if (mode === 'day') {
        var dayToCheck = new Date(date).setHours(0,0,0,0);

        for (var i = 0; i < $scope.events.length; i++) {
          var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

          if (dayToCheck === currentDay) {
            return $scope.events[i].status;
          }
        }
      }

      return '';
    }
  }
]);