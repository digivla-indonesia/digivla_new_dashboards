'use strict';

angular.module('core').controller('HomeController', ['$scope', '$state', 'Authentication',
  function ($scope, $state, Authentication) {
    // This provides Authentication context.
    $scope.authentication = Authentication;
    
    if(Authentication.user === "" || typeof Authentication.user !== "object"){
      $state.go("page.authentication.signin");
    }
  }
]);
