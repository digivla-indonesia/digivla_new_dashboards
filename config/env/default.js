'use strict';

module.exports = {
  app: {
    title: 'Digivla Indonesia',
    description: 'Integrated media monitoring and insight',
    keywords: 'mongodb, express, angularjs, node.js, mongoose, passport',
    googleAnalyticsTrackingID: process.env.GOOGLE_ANALYTICS_TRACKING_ID || 'GOOGLE_ANALYTICS_TRACKING_ID'
  },
  port: process.env.PORT || 3000,
  templateEngine: 'swig',
  // Session Cookie settings
  sessionCookie: {
    // session expiration is set by default to 24 hours
    maxAge: 24 * (60 * 60 * 1000),
    // httpOnly flag makes sure the cookie is only accessed
    // through the HTTP protocol and not JS/browser
    httpOnly: true,
    // secure cookie should be turned to true to provide additional
    // layer of security so that the cookie is set only when working
    // in HTTPS mode.
    secure: false
  },
  // sessionSecret should be changed for security measures and concerns
  sessionSecret: process.env.SESSION_SECRET || 'GvnTeJ4P@6CFz6NJn[<$?h>Xs79S3?Z4',
  // sessionKey is set to the generic sessionId key used by PHP applications
  // for obsecurity reasons
  sessionKey: 'sessionId',
  sessionCollection: 'sessions',
  logo: 'public/img/brand/logo.png',
  favicon: 'public/img/brand/favicon.ico',
  uploads: {
    profileUpload: {
      dest: './public/img/profile/uploads/', // Profile upload destination path
      limits: {
        fileSize: 1*1024*1024 // Max file size in bytes (1 MB)
      }
    }
  },

  // local ip
  apiAddress: 'http://172.16.0.5:8080',
  // elasticAddress: 'http://el.antara-insight.id/',
  // ipElastic: 'el.antara-insight.id',
  elasticAddress: 'http://172.16.0.3/',
  ipElastic: '172.16.0.3',
  sessionAddress: ['172.16.0.3:11211'],
  botApiPost: 'http://128.199.245.199:9080/',
  botApiGet: 'http://128.199.245.199:9070/',
  digitalOceanAPI: 'http://139.59.245.121:3035',
  connectPath: './public/ai_connect/'
};
