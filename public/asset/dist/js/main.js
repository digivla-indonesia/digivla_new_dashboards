var curpage = 1;
$(document).ready(function(){
    $("#index-2").hide();
    $("#index-3").hide();
});

function changeNextPage(){
    if(curpage !== 3){
        $('#index-' + curpage).fadeOut('slow');
        curpage += 1;
        var nextpage = 'index-' + curpage.toString();
        $('#' + nextpage).fadeIn('slow');
    }
}

function changePrevPage(){
    if(curpage !== 1){
        $('#index-' + curpage).fadeOut('slow');
        curpage -= 1;
        var nextpage = 'index-' + curpage.toString();
        $('#' + nextpage).fadeIn('slow');
    }
}

var config = {
    '.chosen-select-no-single' : {disable_search_threshold:10,disable_search:true}
}

for (var selector in config) {
    $(selector).chosen(config[selector]);
}

$("input[name='rank-button']").change(function(){
    var $this = $(this);
    $('#rank-20-img').attr("src", "img/icon-rank-20.png");
    $('#rank-15-img').attr("src", "img/icon-rank-15.png");
    $('#rank-10-img').attr("src", "img/icon-rank-10.png");
    $('#rank-5-img').attr("src", "img/icon-rank-5.png");
    //alert("value: " + $this.val() + '\ntype: ' + typeof $this.val());
    if($this.val() == '20'){
        $('#rank-20-img').attr("src", "img/icon-rank-20-active.png");
    } else if($this.val() == '15'){
        $('#rank-15-img').attr("src", "img/icon-rank-15-active.png");
    } else if($this.val() == '10'){
        $('#rank-10-img').attr("src", "img/icon-rank-10-active.png");
    } else if($this.val() == '5'){
        $('#rank-5-img').attr("src", "img/icon-rank-5-active.png");
    }
});

/* MEDIA VISIBILITY CHART */
var chart = c3.generate({
    bindto: '#media-visibility-chart',
    data: {
        x: 'x',
        columns: [
            ['x', '2015-11-11', '2015-11-12', '2015-11-13', '2015-11-14', '2015-11-15'],
            ['data1', 30, 200, 100, 400, 150, 250],
            ['data2', 130, 100, 140, 200, 150, 50],
            ['data3', 250, 175, 240, 350, 325, 200],
            ['data4', 197, 276, 376, 225, 199, 175],
            ['data5', 187, 474, 240, 306, 209, 50]
        ],
        name: {
            data1: 'Importasi LNG',
            data2: 'Negosiasi Blok',
            data3: 'PGN - Pertagas',
            data4: 'Perombakan Direksi',
            data5: 'Turunnya Laba Pertamina'
        },
        type: 'spline'
    },
    grid: {
        x: {
            show: true
        },
        y: {
            show: true
        }
    },
    axis: {
        x: {
            type: 'timeseries',
            tick: {
                format: function(rd){
                    var month = new Array();
                        month[0] = "Jan";
                        month[1] = "Feb";
                        month[2] = "Mar";
                        month[3] = "Apr";
                        month[4] = "May";
                        month[5] = "Jun";
                        month[6] = "Jul";
                        month[7] = "Aug";
                        month[8] = "Sep";
                        month[9] = "Oct";
                        month[10] = "Nov";
                        month[11] = "Dec";
                        
                    var tgl = rd.getDate();
                    var mon = month[rd.getMonth()];
                    var yr = rd.getFullYear();
                    var xlegend = tgl + '-' + mon + '-' + yr;
                    return xlegend;
                },
                rotate: -40,
                multiline: false
            },
            height: 40
        }
    }
});
/* END OF MEDIA VISIBILITY CHART */

/* COVERAGE TONALITY CHART */
var ctchart = c3.generate({
    bindto: '#coverage-tonality-chart',
    data: {
        x: 'x',
        columns: [
            ['x', '2015-11-11', '2015-11-12', '2015-11-13', '2015-11-14', '2015-11-15'],
            ['data1', 30, 200, 100, 400, 150, 250],
            ['data2', 130, 100, 140, 200, 150, 50],
            ['data3', 250, 175, 240, 350, 325, 200],
            ['data4', 197, 276, 376, 225, 199, 175],
            ['data5', 187, 474, 240, 306, 209, 50]
        ],
        name: {
            data1: 'Importasi LNG',
            data2: 'Negosiasi Blok',
            data3: 'PGN - Pertagas',
            data4: 'Perombakan Direksi',
            data5: 'Turunnya Laba Pertamina'
        },
        type: 'spline'
    },
    axis: {
        x: {
            type: 'timeseries',
            tick: {
                format: function(rd){
                    var month = new Array();
                        month[0] = "Jan";
                        month[1] = "Feb";
                        month[2] = "Mar";
                        month[3] = "Apr";
                        month[4] = "May";
                        month[5] = "Jun";
                        month[6] = "Jul";
                        month[7] = "Aug";
                        month[8] = "Sep";
                        month[9] = "Oct";
                        month[10] = "Nov";
                        month[11] = "Dec";
                        
                    var tgl = rd.getDate();
                    var mon = month[rd.getMonth()];
                    var yr = rd.getFullYear();
                    var xlegend = tgl + '-' + mon + '-' + yr;
                    return xlegend;
                },
                rotate: -40,
                multiline: false
            },
            height: 80
        }
    }   
});
/* END OF COVERAGE TONALITY CHART */

/* TONE BY NEWS CATEGORY CHART */
var ncchart = c3.generate({
    bindto: '#news-category-chart',
    data: {
        x: 'x',
        columns: [
            ['x', '2015-11-11', '2015-11-12', '2015-11-13', '2015-11-14', '2015-11-15', '2015-11-16', '2015-11-17'],
            ['Positif', 6, 15, 5, 5, 7, 8, 1],
            ['Netral', 0, 0, 1, 1, 0, 0, 0],
            ['Negatif', 11, 21, 16, 6, 4, 6, 5],
            ['Garis Total', 17, 36, 22, 12, 11, 14, 6],
        ],
        type: 'bar',
        types: {
            'Garis Total': 'spline',
        },
        groups: [
            ['Positif','Netral', 'Negatif']
        ],
        order: null,
        colors: {
            'Garis Total': '#0094D9'
        },
        label: true
    },
    axis: {
        x: {
            type: 'timeseries',
            tick: {
                format: function(rd){
                    var month = new Array();
                        month[0] = "Jan";
                        month[1] = "Feb";
                        month[2] = "Mar";
                        month[3] = "Apr";
                        month[4] = "May";
                        month[5] = "Jun";
                        month[6] = "Jul";
                        month[7] = "Aug";
                        month[8] = "Sep";
                        month[9] = "Oct";
                        month[10] = "Nov";
                        month[11] = "Dec";
                        
                    var tgl = rd.getDate();
                    var mon = month[rd.getMonth()];
                    var yr = rd.getFullYear();
                    var xlegend = tgl + '-' + mon + '-' + yr;
                    return xlegend;
                },
                rotate: -40,
                multiline: false
            },
            height: 80
        }
    },
    bar: {
        width: {
            ratio: 0.5
        }
    }
});

ncchart.data.colors({
    Positif: '#89B720',
    Negatif: '#D8655A',
    Netral: '#FFFFFF'
});
/* END OF TONE BY NEWS CATEGORY CHART */

/* TONE BY MAINSTREAM MEDIA CHART */
var mmchart = c3.generate({
    bindto: '#mainstream-media-chart',
    data: {
        x: 'x',
        columns: [
            ['x', 'tempo.co.id', 'metrotvnews.com', 'okezone.com', 'beritasatu.com', 'Detik.com', 'inilah.com', 'Suara Karya', 'Koran Tempo', 'bisnis.com', 'antaranews.com', 'Harian Kontan', 'Investor Daily Indonesia', 'Suara Pembaruan', 'Rakyat Merdeka', 'Republika', 'Bisnis Indonesia', 'vivanews.com',
            'Kompas', 'Kedaulatan Rakyat', 'tribunews.com'],
            ['Positif', 6, 7, 1, 3, 1, 3, 2, 2, 4, 2, 2, 0, 1, 3, 3, 2, 0, 0, 0, 2],
            ['Netral', 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            ['Negatif', 10, 5, 10, 6, 6, 3, 3, 3, 0, 3, 4, 3, 2, 1, 0, 1, 3, 2, 2, 0],
        ],
        type: 'bar',
        groups: [
            ['Positif','Netral', 'Negatif']
        ],
        order: null,
        label: true
    },
    axis: {
        x: {
            type: 'category',
            tick: {
                rotate: -40,
                multiline: false
            },
            height: 80
        }
    },
    bar: {
        width: {
            ratio: 0.5
        }
    }
});

mmchart.data.colors({
    Positif: '#89B720',
    Negatif: '#D8655A',
    Netral: '#FFFFFF'
});
/* END OF TONE BY MAINSTREAM MEDIA CHART */

/* EARLY WARNING SYSTEM CHART */
var ewschart = c3.generate({
    bindto: '#early-warning-system-chart',
    data: {
        x: 'x',
        columns: [
            ['x', '2015-11-5', '2015-11-6', '2015-11-7', '2015-11-8', '2015-11-9', '2015-11-10', '2015-11-11'],
            ['data1', 30, 200, 100, 400, 150, 250],
            ['data2', 130, 100, 140, 200, 150, 50],
            ['data3', 250, 175, 240, 350, 325, 200],
            ['data4', 197, 276, 376, 225, 199, 175],
            ['data5', 187, 474, 240, 306, 209, 50]
        ],
        name: {
            data1: 'Revaluasi Aset',
            data2: 'Kerjasama Bechtel Coorporation',
            data3: 'Importasi UNG',
            data4: 'PGN-Pertagas',
            data5: 'Turunnya Laba Pertamina'
        },
        type: 'line'
    },
    axis: {
        x: {
            type: 'timeseries',
            tick: {
                format: function(rd){
                    var month = new Array();
                        month[0] = "Jan";
                        month[1] = "Feb";
                        month[2] = "Mar";
                        month[3] = "Apr";
                        month[4] = "May";
                        month[5] = "Jun";
                        month[6] = "Jul";
                        month[7] = "Aug";
                        month[8] = "Sep";
                        month[9] = "Oct";
                        month[10] = "Nov";
                        month[11] = "Dec";
                        
                    var tgl = rd.getDate();
                    var mon = month[rd.getMonth()];
                    var yr = rd.getFullYear();
                    var xlegend = tgl + '-' + mon + '-' + yr;
                    return xlegend;
                },
                rotate: -40,
                multiline: false
            },
            height: 80
        }
    }   
});
/* END OF EARLY WARNING SYSTEM CHART */