$(document).ready(function() {
    $('.periodcontent').hide();

    $('#select').change(function() {
        $('.periodcontent').hide();
        $('.' + $(this).val()).show();
    }).triggerHandler('change');
});

$(function() {
	$('#popupDatepicker').datepick();
	$('#popupDatepicker2').datepick();

    $( '#dl-menu' ).dlmenu();

    /* TONE BY MAINSTREAM MEDIA CHART */
    var mmchart = c3.generate({
        bindto: '#mainstream-media-chart',
        padding:{
            left: 20,
            right: 15
        },
        data: {
            x: 'x',
            columns: [
                //['x', 'tempo.co.id', 'metrotvnews.com', 'okezone.com', 'beritasatu.com', 'Detik.com', 'inilah.com', 'Suara Karya', 'Koran Tempo', 'bisnis.com', 'antaranews.com', 'Harian Kontan', 'Investor Daily Indonesia', 'Suara Pembaruan', 'Rakyat Merdeka', 'Republika', 'Bisnis Indonesia', 'vivanews.com', 'Kompas', 'Kedaulatan Rakyat', 'tribunews.com'],
                ['x', '1', '2', '3', '4', '5', '6', '7', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19'],
                ['Positif', 6, 7, 1, 3, 1, 3, 2, 2, 4, 2, 2, 0, 1, 3, 3, 2, 0, 0, 0, 2],
                ['Netral', 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                ['Negatif', 10, 5, 10, 6, 6, 3, 3, 3, 0, 3, 4, 3, 2, 1, 0, 1, 3, 2, 2, 0],
            ],
            type: 'bar',
            groups: [
                ['Positif','Netral', 'Negatif']
            ],
            order: null,
            label: true
        },
        axis: {
            x: {
                type: 'category',
                tick: {
                    //rotate: -40,
                    multiline: false
                },
                height: 0
            }
        },
        bar: {
            width: {
                ratio: 0.5
            }
        },
        tooltip: {
            format: {
                title: function (d) {
                    //return 'Data ' + d;
                    var tooltiptitle = ['tempo.co.id', 'metrotvnews.com', 'okezone.com', 'beritasatu.com', 'Detik.com', 'inilah.com', 'Suara Karya', 'Koran Tempo', 'bisnis.com', 'antaranews.com', 'Harian Kontan', 'Investor Daily Indonesia', 'Suara Pembaruan', 'Rakyat Merdeka', 'Republika', 'Bisnis Indonesia', 'vivanews.com', 'Kompas', 'Kedaulatan Rakyat', 'tribunews.com'];

                    return tooltiptitle[d];
                },
                value: function (value, ratio, id) {
                    return value;
                    // var format = id === 'data1' ? d3.format(',') : d3.format('$');
                    // return format(value);
                }
            }
        }
    });

    mmchart.data.colors({
        Positif: '#89B720',
        Negatif: '#D8655A',
        Netral: '#FFFFFF'
    });
    /* END OF TONE BY MAINSTREAM MEDIA CHART */
});

var config = {
	'.chosen-select-no-single' : {disable_search_threshold:10,disable_search:true}
}
for (var selector in config) {
	$(selector).chosen(config[selector]);
}

var checked = false;
function checkAll() {
	var checkboxes = document.getElementsByTagName('input');
	if (!checked) {
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i].type == 'checkbox') {
				checkboxes[i].checked = true;
			}
		}
		checked = true;
	} else {
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i].type == 'checkbox') {
				checkboxes[i].checked = false;
			}
		}
		checked = false;
	}
}

(function($){
	$(window).load(function(){
        $("#result").hide();

		$("#coverage").mCustomScrollbar({
			theme:"rounded-dots"
		});
	});
})(jQuery);