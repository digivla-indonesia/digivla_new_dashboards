<?php
/**
 * PHPPowerPoint
 *
 * Copyright (c) 2009 - 2010 PHPPowerPoint
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPPowerPoint
 * @package    PHPPowerPoint
 * @copyright  Copyright (c) 2009 - 2010 PHPPowerPoint (http://www.codeplex.com/PHPPowerPoint)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
error_reporting(E_ALL); 

/** Include path **/
set_include_path(get_include_path() . PATH_SEPARATOR . './Classes/');

/** PHPPowerPoint */ 
include 'PHPPowerPoint.php';  
 
// Create new PHPPowerPoint object 
$objPHPPowerPoint = new PHPPowerPoint(); 
// Set properties 
$objPHPPowerPoint->getProperties()->setCreator("Digivla")
								  ->setLastModifiedBy("Digivla")
								  ->setTitle("Chart Presentation Digivla")
								  ->setSubject("Chart Presentation Digivla")
								  ->setDescription("Chart Presentation Digivla")
								  ->setKeywords("Chart Presentation Digivla")
								  ->setCategory("Digivla");

// Remove first slide 
$objPHPPowerPoint->removeSlideByIndex(0);


/**get parameter**/

$chart  = (array_key_exists('chart',$_POST)) ? $_POST['chart'] : false;
$_title = (array_key_exists('title',$_POST)) ? $_POST['title'] : false;
$cliset = $_POST['cliset'];
$subcli = $_POST['subcli'];
$timeframe = $_POST['timeframeX'];
$from = $_POST['fromX'];
$to = $_POST['toX'];

//cek isi
if($chart === false)
{
	exit("Not Allowed");
}
//kategori
if($subcli=='all')
{
	$cliset = $cliset; 
}
else
{
	$cliset = $cliset." (".$subcli.")";
}

//tanggal
if(empty($from) and empty($to))
{
	$date_now = date('Y-m-d');
	$dateene1 = strtotime($date_now);
	$datene1 = date('d/M/Y',$dateene1);
	$date_par = date('Y-m-d', strtotime('-'.($timeframe+1).' day', strtotime(date('Y-m-d'))));
	$dateene2 = strtotime($date_par);
	$datene2 = date('d/M/Y',$dateene2);
	$timeframe = $datene2." - ".$datene1;
}
else
{
	$date_now = $from;
	$dateene1 = strtotime($date_now);
	$datene1 = date('d/M/Y',$dateene1);
	$date_now2 = $to;
	$dateene2 = strtotime($date_now2);
	$datene2 = date('d/M/Y',$dateene2);
	$timeframe = $datene1." - ".$datene2;
}


//create first page
$currentSlide = createTemplatedSlide($objPHPPowerPoint,'', 'first', '.');
//text				   
$shape = $currentSlide->createRichTextShape()
      ->setHeight(250)
      ->setWidth(920)
      ->setOffsetX(10)
      ->setOffsetY(150)
      ->setInsetTop(20)
      ->setInsetBottom(50);
$shape->getActiveParagraph()->getAlignment()->setHorizontal( PHPPowerPoint_Style_Alignment::HORIZONTAL_CENTER );
$textRun = $shape->createTextRun('Analisa Pemberitaan');
$textRun->getFont()->setBold(true);
$textRun->getFont()->setSize(35);
$textRun->getFont()->setColor( new PHPPowerPoint_Style_Color( '000000' ) );
$shape->createBreak();
$textRun = $shape->createTextRun($cliset);
$textRun->getFont()->setBold(true);
$textRun->getFont()->setSize(30);
$textRun->getFont()->setColor( new PHPPowerPoint_Style_Color( '000000' ) );
$shape->createBreak();
$textRun = $shape->createTextRun("Tanggal :  ".$timeframe);
$textRun->getFont()->setBold(true);
$textRun->getFont()->setSize(30);
$textRun->getFont()->setColor( new PHPPowerPoint_Style_Color( '000000' ) );
//----------------

$rand_time =	time();
foreach($chart as $idx => $imgbase64)
{ 
	$title 		= $_title[$idx];
	$imgname 	= "chart_{$idx}_{$rand_time}.png"; 
	file_put_contents('./images/'.$imgname,file_get_contents("data://".$imgbase64));  
	usleep(1000); 
	
	$currentSlide = createTemplatedSlide($objPHPPowerPoint,$imgname, 'isine', $timeframe);
	$shape = $currentSlide->createRichTextShape()
      ->setHeight(250)
      ->setWidth(920)
      ->setOffsetX(10)
      ->setOffsetY(50)
      ->setInsetTop(20)
      ->setInsetBottom(50);
	$shape->getActiveParagraph()->getAlignment()->setHorizontal( PHPPowerPoint_Style_Alignment::HORIZONTAL_CENTER );

	$textRun = $shape->createTextRun($title);
	$textRun->getFont()->setBold(true)
					   ->setSize(25)
					   ->setColor(new PHPPowerPoint_Style_Color('000000'));
					   
	
}
	
 
// Save PowerPoint 2007 file
//echo date('H:i:s') . " Write to PowerPoint2007 format\n";
$filepptname = 'export_ppt_'.$rand_time;
$objWriter = PHPPowerPoint_IOFactory::createWriter($objPHPPowerPoint, 'PowerPoint2007');
//$objWriter->setLayoutPack(new PHPPowerPoint_Writer_PowerPoint2007_LayoutPack_TemplateBased('./resources/template.pptx'));
$objWriter->save('./ppt/'.$filepptname.'.pptx');
$pathx = curPageURL();   
$myfilename = basename(__FILE__, '.php'); 
$path = str_replace($myfilename.".php","",$pathx); 

echo "<script>window.open('{$path}/ppt/{$filepptname}.pptx')</script>";
foreach($chart as $idx => $imgbase64)
{ 
	$imgname 	= "chart_{$idx}_{$rand_time}.png";
	unlink('./images/'.$imgname);
}
 
/**
 * Creates a templated slide
 *
 * @param PHPPowerPoint $objPHPPowerPoint
 * @return PHPPowerPoint_Slide
 */
function createTemplatedSlide(PHPPowerPoint $objPHPPowerPoint,$imgfile, $state, $timeframe)
{
	// Create slide
	$slide = $objPHPPowerPoint->createSlide(); 
    
    $slide->createDrawingShape()
          ->setName('Background')
          ->setDescription('Background') 
          ->setPath('./resources/backgroundne.gif')
          ->setWidth(950)
          ->setHeight(720)
          ->setOffsetX(0)
          ->setOffsetY(0);
	
	//if(isset($_POST['logo_compX']))
	if(isset($_POST['logo_comp']))
	{
		$shape = $slide->createDrawingShape();
		$shape->setName('logo')
			  ->setDescription('Logo')
			  ->setPath('../../admin/asset/images/'.$_POST['logo_comp'].'')
			  ->setHeight(20)
			  ->setWidth(140)
			  ->setOffsetX(10)
			  ->setOffsetY(10);
		$shape->getShadow()->setVisible(true)
						   ->setDirection(45)
						   ->setDistance(10);
	}
	
	$shape = $slide->createDrawingShape();
	$shape->setName('line')
		  ->setDescription('line')
		  ->setPath('./resources/garise.png')
		  ->setHeight(5)
		  //->setWidth(800)
		  ->setWidth(900)
		  //->setOffsetX(150)
		  ->setOffsetX(30)
		  ->setOffsetY(45);
    
	if($state=='isine')
	{
    $slide->createDrawingShape()
          ->setName($imgfile)
          ->setDescription($imgfile)
          ->setPath('./images/'.$imgfile)
          ->setWidth(850)	
          ->setOffsetX(60)
          ->setOffsetY(150); 
	}
	
	// Add tanggal footer
    $shape = $slide->createRichTextShape()
      ->setHeight(30)
      ->setWidth(920)
      ->setOffsetX(10)
      ->setOffsetY(690)
      ->setInsetTop(20)
      ->setInsetBottom(50);
	$shape->getActiveParagraph()->getAlignment()->setHorizontal( PHPPowerPoint_Style_Alignment::HORIZONTAL_LEFT );
	$textRun = $shape->createTextRun($timeframe);
	$textRun->getFont()->setBold(true);
	$textRun->getFont()->setSize(12);
	$textRun->getFont()->setColor( new PHPPowerPoint_Style_Color( '9E9B9C' ) );
	
    return $slide;
}

function curPageURL() {
 $pageURL = 'http';
 //if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 } 
 return $pageURL;
}
 
